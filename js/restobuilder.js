/**
 * Created by xdire on 27.02.15.
 */

(function() {

    var $cr = function () {
        return new RBF();
    };

    var RBF = function () {

        this.lastOpenedRestaurant=null;

        this.listPane=null;
        this.infoPane=null;

        this.listPaneListFrame=null;
        this.listPaneCtrlFrame=null;

        this.listOfImagesToUpload=[];
        this.listOfUploadedImages=[];

        this.open = function(e)
        {
            if(window.XWF && window.MB){
                openRestaurantPane(e);
            } else {
                alert('Module - Frames and Module - MenuBuilder are misconfigured, cannot continue');
            }

        };

        this.deleteRestoImg = function(e,id){
            RBDeleteRestoImageButtonQuestion(e,id);
        };
        this.deleteRestoImgEnd = function(e,id){
            RBDeleteRestoImageButtonRemove(e,id);
        }

    };

    function openRestaurantPane()
    {
        XWF.connQueryObject = {query:"admindata", type:"getrestolist"};
        XWF.connQueryData = {ident:0,user:0,data:""};

        $cine().userkey(XWF.connQueryData);

        XWF.send({
            navigationBar:true,
            waitCallback:true,
            raiseWindow:true,
            raiseWindowID:"resto-window",
            onComplete:procRestoList,
            changeTitle:'Restaurants',
            windowSubTitle:"List"
        });
    }

    function procRestoList(data){

        var c1,c2,l,lc,i,k,r,c,el,t;

        var obj=data.data;
        var o=data.content;

        c1=document.createElement('div');
        c1.style.cssText='position:relative; width:40%; height:100%; padding:0 1% 0 1%; min-height:250px; display:inline-block; vertical-align:top; overflow-y: scroll; overflow-x:hidden';

        lc=document.createElement('div');
        lc.style.cssText='width:100%; height:64px; overflow:hidden; margin-right: -25px; padding-right:25px; box-sizing:initial;';
        c1.appendChild(lc);

        RBRestaurantControls(lc);

        l=document.createElement('div');
        l.style.cssText='width:100%; height:auto; overflow-y: scroll; margin-right: -25px; padding-right:25px; box-sizing:initial;';
        c1.appendChild(l);

        RB.listPaneCtrlFrame=lc;
        RB.listPaneListFrame=l;
        RB.listPane=c1;

        c2=document.createElement('div');
        c2.style.cssText='width:60%; height:100%; padding:0 1% 0 1%; border-left:1px #aaaaaa dotted; min-height:250px; display:inline-block; vertical-align:top; overflow-x:hidden; overflow-y: hidden;';
        i=document.createElement('div');
        i.style.cssText='position:relative; width:99%; height:100%; text-align:center; padding-left:1%; display:inline-block; vertical-align:top; overflow-y: scroll; margin-right: -25px; padding-right:25px; box-sizing:initial;';
        c2.appendChild(i);
        RB.infoPane=i;

        o.appendChild(c1);
        o.appendChild(c2);

        if(obj.success){
            r=obj.result;
            if(r){

                for(k in r){

                    c=r[k];
                    l.appendChild(RBRestaurantListElement(k,c['name']));

                }

            }

        }

    }

    function RBRestaurantListElement(id,name){

        var el=document.createElement('div');
        el.className='cine-list-item';
        el.setAttribute('id','c_resto'+id);

        var t=document.createElement('div');
        t.className='cine-list-item-f c-list-id';
        t.innerHTML=id;
        el.appendChild(t);

        t=document.createElement('div');
        t.className='cine-list-item-f c-list-date';
        t.innerHTML=name;
        el.appendChild(t);

        el.addEventListener('click',function(e){cGetRestourant(e,this)}.bind(id));
        return el;

    }

    function RBRestaurantControls(parent){

        var addResto=document.createElement('div');
        addResto.className='cine_fn_color_btn w100';
        addResto.textContent='Add restaurant';
        addResto.addEventListener('click',function(e){RBrestaurantAddNew(e)});

        parent.appendChild(addResto);

    }

    function cGetRestourant(e,id){

        XWF.connQueryObject = {query:"admindata", type:"getresto"};
        XWF.connQueryData = {ident:id,user:0,data:""};

        $cine().userkey(XWF.connQueryData);

        XWF.send({
            waitCallback:true,
            onComplete:procRestoInfo
        });

    }

    function procRestoInfo(data){

        var o,r,b,c,i,t;
        o=RB.infoPane;
        //{"name":"La Tarte Flamb","city":"0","type":"3","email":"shaneapptester@gmail.com",
        // "r_phone":"7185028971","zip":"10016","lat":"40.745891","lng":"-73.980053",
        // "address":{"address_country":"","address_state":"","address_city":"","address_street":"","address_zip":"","address_room":""},
        // "desc":"","img":{"main":0},"regdate":"0000-00-00 00:00:00","token":"","merchant":"","manage":{"18":1}}
        var obj=data.data;

        if(obj.success){

            // Title
            cineFnClearDOM(o);
            r=obj.result;
            RB.lastOpenedRestaurant=r.id;

            c=document.createElement('div');
            c.innerHTML='<h2>'+r.name+'</h2>';
            o.appendChild(c);

            // RESTAURANT STATE AND MERCHANT / PARTNER STATUS
            table=document.createElement('table');
            table.className='table_elements';

            tr=table.insertRow(0);
            td=tr.insertCell(0);
            td.textContent='Operational status';
            td=tr.insertCell(1);
            td.appendChild($cine().toggler(RBrestaurantSwitchOn,RBrestaurantSwitchOff,(r.status==1)));
            // Merchant
            tr=table.insertRow(1);
                td=tr.insertCell(0); td.textContent='Merchant Status';
                td=tr.insertCell(1); td.textContent=(r['merchant'] != 0)?'Registered and active':'Not registered';
            // Partner
            tr=table.insertRow(2);
                td=tr.insertCell(0); td.textContent='Partner Status';
                partners=['Normal','Beta','Alpha'];
                td=tr.insertCell(1); td.textContent=partners[r['partner']];

            o.appendChild(table);

            // OPERATIONAL BUTTONS FOR EXECUTE RESTAURANT FUNCTIONS
            c=document.createElement('div');

            b=document.createElement('button');
            b.className="cine_fn_color_btn";
            b.style.cssText='width:96%;';
            b.innerHTML='Edit Restaurant General Information';
            b.addEventListener('click',function(e){RBrestaurantEdit(e,this);}.bind(r.id));
            c.appendChild(b);

            b=document.createElement('button');
            b.className="cine_fn_color_btn";
            b.style.cssText='width:96%;';
            b.innerHTML='Edit Restaurant Details';
            b.addEventListener('click',function(e){RBrestaurantEditDetails(e,this);}.bind(r.id));
            c.appendChild(b);

            b=document.createElement('button');
            b.className="cine_fn_color_btn";
            b.style.cssText='width:96%;';
            b.innerHTML='Edit restaurant menu and sides';
            b.addEventListener('click',function(e){MB.restaurant=this; MB.open(e);}.bind(r.id));
            c.appendChild(b);

            b=document.createElement('button');
            b.className="cine_fn_color_btn";
            b.style.cssText='width:96%;';
            b.innerHTML='Tree menu editor';
            b.addEventListener('click',function(e){MB.restaurant=this; MB.openTree(e);}.bind(r.id));
            c.appendChild(b);

            b=document.createElement('button');
            b.className="cine_fn_color_btn";
            b.style.cssText='width:96%;';
            b.innerHTML='Test Braintree Merchant Account';
            b.addEventListener('click',function(e){cTestSubmerchant(this.id)}.bind({id:r.id}));
            c.appendChild(b);

            b=document.createElement('button');
            b.className="cine_fn_color_btn";
            b.style.cssText='width:96%;';
            b.innerHTML='Add Braintree Merchant Account';
            b.addEventListener('click',function(e){cEditSubmerchant(this.id)}.bind({id:r.id}));
            c.appendChild(b);

            o.appendChild(c);


        }


    }

    function RBrestaurantAddNew(e){

        var win=cOpenTinyModal({appTitle:"Add new restaurant"});
        var control=document.createElement('div');
        var button=document.createElement('div');
        control.style.cssText='position:relative; height:85%; overflow-y:scroll; width:100%;';
        button.style.cssText='position:relative; height:15%; overflow:hidden; width:100%;';

        win.content.appendChild(control);
        win.content.appendChild(button);

        saveBtn=document.createElement('button');
        saveBtn.className='cine_fn_color_btn';
        saveBtn.textContent='Save';
        saveBtn.addEventListener('click', function (e) {
            RBrestaurantAddNewSave();
        });

        cancelBtn=document.createElement('button');
        cancelBtn.className='cine_fn_color_btn';
        cancelBtn.textContent='Cancel';
        cancelBtn.addEventListener('click',function(e){cRemoveTinyModal();});

        button.appendChild(saveBtn);
        button.appendChild(cancelBtn);

        mainFrame=RBrestaurantInfoWindowFrame();
        control.appendChild(mainFrame);

    }

    function RBrestaurantAddNewSave(){

        var name=document.getElementById('radd_name');

        if(name!=null){

            var owner=document.getElementById('radd_owner');
            var state=document.getElementById('radd_state');
            var city=document.getElementById('radd_city');
            var street=document.getElementById('radd_street');
            var zip=document.getElementById('radd_zip');
            var phone=document.getElementById('radd_phone');
            var latitude=document.getElementById('radd_latitude');
            var longitude=document.getElementById('radd_longitude');
            var email1=document.getElementById('radd_email1');
            var email2=document.getElementById('radd_email2');
            var description=document.getElementById('radd_description');
            var type=document.getElementById('radd_type');

            var data={
                name:name.value,
                owner:owner.value,
                state:state.value,
                city:city.value,
                street:street.value,
                zip:zip.value,
                phone:phone.value,
                latitude:latitude.value,
                longitude:longitude.value,
                email1:email1.value,
                email2:email2.value,
                description:description.value,
                type:type.value
            };

            XWF.connQueryObject = {query:"admindata", type:"putnewrestaurant"};
            XWF.connQueryData = {data:JSON.stringify(data)};

            $cine().userkey(XWF.connQueryData);

            XWF.send({
                waitCallback:true,
                onComplete:RBrestaurantAddNewSaveFinish
            });

        }

    }

    function RBrestaurantAddNewSaveFinish(data){

        var obj=data.data;
        /*
         {"success":true,
         "result":
            {"id":17,"name":"My new restaurant","status":0,"city":"27795","type":0,"email":"dire.one@gmail.com",
            "phone":"16468740014","zip":"10016","lat":"40.0001","lng":"-73.0001",
            "address":
                "{\"address_country\":\"United States\",\"address_state\":\"NY\",\"address_city\":\"New York\",\"address_street\":\"31 East 32nd Street\",\"address_zip\":\"10016\",\"address_room\":\"\"}",
            "desc":"HUH???","img":"","merchant":0,"partner":0,"regdate":"2015-03-02 18:16:25",
            "token":"","merchant_info":"","manage":"{\"1\":1}"},"message":""}
        */
        if(obj.success){

            var resto=obj.result;
            var first=RB.listPaneListFrame.firstChild;
            RB.listPaneListFrame.insertBefore(RBRestaurantListElement(resto.id,resto.name),first);
            cRemoveTinyModal();

        }

    }

    function RBrestaurantEdit(e,id){

        XWF.connQueryObject = {query:"admindata", type:"getresto"};
        XWF.connQueryData = {ident:id,data:1};

        $cine().userkey(XWF.connQueryData);

        XWF.send({
            waitCallback:true,
            onComplete:RBrestaurantEditOpen
        });

    }

    function RBrestaurantEditOpen(data){

        var obj=data.data;

        if(obj.success){

            var win=cOpenTinyModal({appTitle:"Edit restaurant"});
            var control=document.createElement('div');
            var button=document.createElement('div');
            control.style.cssText='position:relative; height:85%; overflow-y:scroll; width:100%;';
            button.style.cssText='position:relative; height:15%; overflow:hidden; width:100%;';

            win.content.appendChild(control);
            win.content.appendChild(button);

            saveBtn=document.createElement('button');
            saveBtn.className='cine_fn_color_btn';
            saveBtn.textContent='Save';
            saveBtn.addEventListener('click', function (e) {
                RBrestaurantEditPush(e,this);
            }.bind(obj.result['id']));

            cancelBtn=document.createElement('button');
            cancelBtn.className='cine_fn_color_btn';
            cancelBtn.textContent='Cancel';
            cancelBtn.addEventListener('click',function(e){cRemoveTinyModal();});

            button.appendChild(saveBtn);
            button.appendChild(cancelBtn);

            mainFrame=RBrestaurantInfoWindowFrame(obj.result);
            control.appendChild(mainFrame);

        }

    }

    function RBrestaurantEditPush(e,id){

        var name=document.getElementById('radd_name');

        if(name!=null)
        {

            var owner=document.getElementById('radd_owner');
            var state=document.getElementById('radd_state');
            var city=document.getElementById('radd_city');
            var street=document.getElementById('radd_street');
            var zip=document.getElementById('radd_zip');
            var phone=document.getElementById('radd_phone');
            var latitude=document.getElementById('radd_latitude');
            var longitude=document.getElementById('radd_longitude');
            var email1=document.getElementById('radd_email1');
            var email2=document.getElementById('radd_email2');
            var description=document.getElementById('radd_description');
            var type=document.getElementById('radd_type');

            var data={
                name:name.value,
                owner:owner.value,
                state:state.value,
                city:city.value,
                street:street.value,
                zip:zip.value,
                phone:phone.value,
                latitude:latitude.value,
                longitude:longitude.value,
                email1:email1.value,
                email2:email2.value,
                description:description.value,
                type:type.value
            };

            XWF.connQueryObject = {query:"admindata", type:"puteditrestaurant"};
            XWF.connQueryData = {ident:id,data:JSON.stringify(data)};

            $cine().userkey(XWF.connQueryData);

            XWF.send({
                waitCallback:true,
                onComplete:RBrestaurantAddNewEditPushFinish
            });

        }
    }

    function RBrestaurantAddNewEditPushFinish(data){

        var obj=data.data;
        if(obj.success){
            cRemoveTinyModal();
        }

    }

    function RBrestaurantSwitchOn(ev,togglerFn){

        XWF.connQueryObject = {query:"admindata", type:"changerestostate"};
        XWF.connQueryData = {ident:RB.lastOpenedRestaurant,user:0,data:1};

        $cine().userkey(XWF.connQueryData);

        XWF.send({
            waitCallback:true,
            onComplete:RBrestaurantSwitchState,
            returnObject:togglerFn
        });

    }

    function RBrestaurantSwitchOff(ev,togglerFn){

        XWF.connQueryObject = {query:"admindata", type:"changerestostate"};
        XWF.connQueryData = {ident:RB.lastOpenedRestaurant,user:0,data:0};

        $cine().userkey(XWF.connQueryData);

        XWF.send({
            waitCallback:true,
            onComplete:RBrestaurantSwitchState,
            returnObject:togglerFn
        });

    }

    function RBrestaurantSwitchState(data){

        var obj=data.data;
        if(obj.success){
            if(data.object){
                data.object();
            }
        }

    }

    function RBrestaurantInfoWindowFrame(data){

        if(typeof data==='undefined')
        {
            data = {
                name:'', status:0, city:0,
                type:0,owner_email:'', email:'',email2:'', phone:'',
                zip:'', lat:'', lng: '',
                address:{address_country:'',address_state:'',address_city:'',address_street:'',address_zip:'',address_room:''},
                desc:'',img:''
            }
        }

        var mainFrame=document.createElement('div');

        var mainLeft=document.createElement('div');
        mainLeft.className='cine_form_green';
        mainLeft.style.cssText='display:inline-block; vertical-align:top; padding:10px; width:50%; text-align:left;';

        var mainRight=document.createElement('div');
        mainRight.className='cine_form_green';
        mainRight.style.cssText='display:inline-block; vertical-align:top; padding:10px; width:50%; text-align:left;';

        // LEFT COLUMN
        // Name
        var label=document.createElement('label');
        label.textContent='Restaurant Name';
        var input=document.createElement('input');
        input.setAttribute('id','radd_name');
        input.setAttribute('placeholder','Title name of restaurant');
        input.setAttribute('value',data.name);
        mainLeft.appendChild(label);
        mainLeft.appendChild(input);
        // Owner
        label=document.createElement('label');
        label.textContent='Owner account';
        input=document.createElement('input');
        input.setAttribute('id','radd_owner');
        input.setAttribute('value',data['owner_email']);
        input.setAttribute('placeholder','Email address of existed user');
        mainLeft.appendChild(label);
        mainLeft.appendChild(input);
        // State
        label=document.createElement('label');
        label.textContent='Restaurant USA State (short title)';
        input=document.createElement('input');
        input.setAttribute('id','radd_state');
        input.setAttribute('value',data.address['address_state']);
        input.setAttribute('placeholder','Ex: NY ');
        mainLeft.appendChild(label);
        mainLeft.appendChild(input);
        // City
        label=document.createElement('label');
        label.textContent='Restaurant USA City (full title)';
        input=document.createElement('input');
        input.setAttribute('id','radd_city');
        input.setAttribute('value',data.address['address_city']);
        input.setAttribute('placeholder','Ex: New York City');
        mainLeft.appendChild(label);
        mainLeft.appendChild(input);
        // Street
        label=document.createElement('label');
        label.textContent='Street Address';
        input=document.createElement('input');
        input.setAttribute('id','radd_street');
        input.setAttribute('value',data.address['address_street']);
        input.setAttribute('placeholder','Ex: 160 Prince street');
        mainLeft.appendChild(label);
        mainLeft.appendChild(input);
        // ZIP
        label=document.createElement('label');
        label.textContent='Zip Area Code';
        input=document.createElement('input');
        input.setAttribute('id','radd_zip');
        input.setAttribute('value',data.address['address_zip']);
        input.setAttribute('placeholder','Ex: 10001');
        mainLeft.appendChild(label);
        mainLeft.appendChild(input);
        // RIGHT COLUMN
        // PHONE
        label=document.createElement('label');
        label.textContent='Phone Number';
        input=document.createElement('input');
        input.setAttribute('id','radd_phone');
        input.setAttribute('value',data['phone']);
        input.setAttribute('placeholder','10 Digit: 15556678899');
        mainRight.appendChild(label);
        mainRight.appendChild(input);
        // LATITUDE
        label=document.createElement('label');
        label.textContent='Latitude';
        input=document.createElement('input');
        input.setAttribute('id','radd_latitude');
        input.setAttribute('value',data['lat']);
        input.setAttribute('placeholder','40.000000');
        mainRight.appendChild(label);
        mainRight.appendChild(input);
        // LONGITUDE
        label=document.createElement('label');
        label.textContent='Longitude';
        input=document.createElement('input');
        input.setAttribute('id','radd_longitude');
        input.setAttribute('value',data['lng']);
        input.setAttribute('placeholder','-73.000000');
        mainRight.appendChild(label);
        mainRight.appendChild(input);
        // EMAIL1
        label=document.createElement('label');
        label.textContent='Primary Email';
        input=document.createElement('input');
        input.setAttribute('id','radd_email1');
        input.setAttribute('value',data['email']);
        input.setAttribute('placeholder','');
        mainRight.appendChild(label);
        mainRight.appendChild(input);
        // EMAIL2
        label=document.createElement('label');
        label.textContent='Service Email';
        input=document.createElement('input');
        input.setAttribute('id','radd_email2');
        input.setAttribute('value',data['email2']);
        input.setAttribute('placeholder','');
        mainRight.appendChild(label);
        mainRight.appendChild(input);
        // DESCRIPTION
        label=document.createElement('label');
        label.textContent='Description';
        input=document.createElement('input');
        input.setAttribute('id','radd_description');
        input.setAttribute('value',data['desc']);
        input.setAttribute('placeholder','Ex: Mediterrain restaurant');
        mainRight.appendChild(label);
        mainRight.appendChild(input);
        // TYPE
        label=document.createElement('label');
        label.textContent='Type of restaurant';
        select=document.createElement('select');
        select.setAttribute('id','radd_type');
        select.style.cssText="width:100%; font-size:24px;";
        length=cineRestoTypes.length;
        type=parseInt(data['type']);
        for(var i=0;i<length;i++){
            option=document.createElement('option');
            option.setAttribute('value',i);
            option.textContent=cineRestoTypes[i];
            if(i==type){
                option.setAttribute('selected','selected');
            }
            select.appendChild(option);
        }
        mainRight.appendChild(label);
        mainRight.appendChild(select);

        mainFrame.appendChild(mainLeft);
        mainFrame.appendChild(mainRight);

        return mainFrame;

    }

    function RBModalInfoWindow(){

        var o=document.createElement('div');
        o.style.cssText='position:absolute; width:100%; background:#f8f8f8; left:0; top:0; height:100%;';
        o.setAttribute('id','modal-info-pane');

        var c=document.createElement('div');
        c.style.cssText='padding-right:25px; width:100%; height:50px;';
        var b=document.createElement('button');
        o.style.textAlign='left';
        b.className="cine_fn_color_btn cine_short_btn";
        b.style.cssText='float:right; width:96px !important; margin-right:25px;';
        b.innerHTML='Close';
        b.addEventListener('click',function(e){this.parentNode.removeChild(this);}.bind(o));
        c.appendChild(b);
        o.appendChild(c);

        c=document.createElement('div');
        c.setAttribute('id','merchant_data');
        c.style.cssText='padding-right:25px; width:100%; height:auto; text-align:center;';

        o.appendChild(c);

        return o;
    }





    // -------------------------------------------------------------------
    //
    //      RESTAURANT EDIT DETAILS AND LINKED IMAGES
    //
    // -------------------------------------------------------------------

    function RBrestaurantEditDetails(e,id){

        XWF.connQueryObject = {query:"admindata", type:"getrestodetail"};
        XWF.connQueryData = {ident:id,data:1};

        $cine().userkey(XWF.connQueryData);

        XWF.send({
            waitCallback:true,
            onComplete:RBrestaurantEditDetailsOpen,
            navigationBar:true,
            raiseWindow:true,
            raiseWindowID:"resto-window-detail",
            changeTitle:'Edit Restaurant',
            windowSubTitle:"Details"
        });

    }

    var uploadImagesLayer=null;
    var uploadedImagesLayer=null;
    var uploadButton=null;
    var textButton=null;

    function RBrestaurantEditDetailsOpen(data){

        var obj=data.data;
        RB.listOfImagesToUpload=[];
        RB.listOfUploadedImages=[];

        uploadImagesLayer=null;
        uploadedImagesLayer=null;

        if(obj.success){

            //console.log(obj.result);
            var resto=obj.result;

            if(resto)
            {
                uploadedImagesLayer=document.createElement('div');
                var uploadImgLayer=document.createElement('div');
                var uploadImgFrame=document.createElement('div');
                var descLayer=document.createElement('div');

                if(resto.image!=null){

                    images=resto.image;

                    for(var k in images){

                        if(images.hasOwnProperty(k))
                        {

                            container=document.createElement('div');
                            container.style.cssText = 'width:192px; height:192px; max-height:192px; max-width:192px; display: inline-block; position:relative; overflow:hidden';
                            container.setAttribute('id','restoimage'+k);
                            container.addEventListener('click',function(e){RBDeleteRestoImageButtonQuestion(e,this)}.bind(k));

                            image = document.createElement('img');
                            image.setAttribute('src',appImagePath + images[k].path);
                            image.style.cssText = "width:auto; height:auto; max-width:192px; max-height:192px;";

                            container.appendChild(image);
                            uploadedImagesLayer.appendChild(container);

                        }

                    }

                } else {

                }

                uploadedImagesLayer.style.cssText = 'height: 192px; width: 98%; border-radius: 8px; background-color: rgba(20,20,20,0.2); display: inline-block; margin-top: 10px; margin-bottom: 10px;';
                uploadImgLayer.style.cssText = 'height:256px; width:100%;';
                uploadImgFrame.style.cssText = 'height: 192px; width: 98%; display: inline-block; border: 1px solid #AAAAAA; border-radius: 8px; margin-left: 5px; margin-right: 5px;';
                descLayer.style.cssText = 'height:128px; width:100%; background-color:#dedede';

                uploadImagesLayer=uploadImgFrame;
                uploadImgLayer.appendChild(uploadImgFrame);
                RBRestaurantEditDetailsUploadBtn(uploadImgLayer);

                label=document.createElement('label');
                label.textContent='Uploaded Images';
                data.content.appendChild(label);
                data.content.appendChild(uploadedImagesLayer);
                label=document.createElement('label');
                label.textContent='Select images to upload';
                data.content.appendChild(label);
                data.content.appendChild(uploadImgLayer);
                data.content.appendChild(descLayer);

            }

        }

    }

    function RBRestaurantEditDetailsUploadBtn(parentLayer){

        textButton=document.createElement('button');
        textButton.className='cine_fn_color_btn';
        textButton.textContent='Select Images';

        uploadButton=document.createElement('button');
        uploadButton.className='cine_fn_color_btn';
        uploadButton.textContent='Upload Images';
        uploadButton.style.display='none';

        var fileButton=document.createElement('input');
        fileButton.style.display='none';
        fileButton.setAttribute('value','Select file on your computer');
        fileButton.setAttribute('type','file');
        fileButton.setAttribute('name','files');
        fileButton.setAttribute('accept','image/*');
        fileButton.setAttribute('multiple','');

        textButton.addEventListener('click',function(e)
        {
            var event = new MouseEvent('click', {
                'view': window,
                'bubbles': true,
                'cancelable': true
            });
            fileButton.dispatchEvent(event);
        });

        fileButton.addEventListener('change',function(e)
        {

            var files=e.target.files;
            var len=files.length;

            for(var i=0;i<len;i++){

                var cur = files[i];
                cur['imageIndexNum']=i;
                //console.log(cur);
                var read=new FileReader();
                read.onload = function(e) {

                    //console.log(e.target);

                    newImageData={name:this.name,type:this.type,size:this.size,data:e.target.result};
                    RB.listOfImagesToUpload.push(newImageData);

                    img=document.createElement('img');
                    img.setAttribute('id','uploadimage'+this.imageIndexNum);
                    img.style.cssText='width:auto; max-width:128px; height:auto; max-height:192px; display:inline-block;';
                    img.src=e.target.result;

                    uploadImagesLayer.appendChild(img);
                    console.log(newImageData);

                }.bind(cur);

                read.readAsDataURL(cur);

            }

            textButton.style.display='none';
            uploadButton.style.display='inline-block';

        });

        uploadButton.addEventListener('click',function(e){
            RBSentUploadImages();
        });

        parentLayer.appendChild(fileButton);
        parentLayer.appendChild(textButton);
        parentLayer.appendChild(uploadButton);

    }

    function RBSentUploadImages(){

        var filesLength=RB.listOfImagesToUpload.length;
        var i,c;

        for(i=0;i<filesLength;i++){

            c=RB.listOfImagesToUpload[i];

            var xhr = new XMLHttpRequest();

            xhr.onreadystatechange = function(ev){

                if (this.xhr.readyState == 4 && this.xhr.status == 200) {
                    if (this.xhr.status == 200) {

                        answer=this.xhr.responseText;
                        RBSentUploadDidRecieveAnswer(this.index,answer);

                    }
                }

            }.bind({xhr:xhr,index:i});

            xhr.open('POST',XWF.connServer+'?query=admindata&type=putrestoimage', true);
            xhr.setRequestHeader("X-Requested-With","XMLHttpRequest");
            xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");

            var credentials=$cine().userkeyupd({});
            var data = 'user='+ credentials.user +'&userkey='+ credentials.userkey +'&ident='+ RB.lastOpenedRestaurant +'&name='+ c.name +'&ext='+ c.type +'&size='+ c.size +'&image=' + c.data;
            xhr.send(data);

        }

    }

    function RBSentUploadDidRecieveAnswer(index,data){

        RB.listOfImagesToUpload[index] = undefined;
        var filesLength=RB.listOfImagesToUpload.length;
        var domImage=document.getElementById('uploadimage'+index);

        if(domImage!=null){
            domImage.parentNode.removeChild(domImage);
        }

        var exist=0;
        for(var i=0;i<filesLength;i++){
            if(RB.listOfUploadedImages[i] != undefined){
                exist++;
            }
        }

        if(exist==0)
        {
            RB.listOfUploadedImages=[];
            textButton.style.display='inline-block';
            uploadButton.style.display='none';
            uploadImagesLayer.style.display='none';
            uploadImagesLayer.offsetHeight;
            uploadImagesLayer.style.display='block';
        }

        console.log(data);
        console.log(index);

        var object=null;
        try {
            object=JSON.parse(data);
        } catch (err){
            console.log(err);
        }

        console.log(object);
        if(object!=null){

            if(object.success) {

                var res=object.result;

                if(res && res.id != 0) {

                    container = document.createElement('div');
                    container.style.cssText = 'width:auto; height:auto; min-height:128px; min-width:128px; max-height:192px; max-width:192px; display: inline-block; position:relative; overflow:hidden';
                    container.setAttribute('id', 'restoimage' + res.id);
                    container.addEventListener('click', function (e) {
                        RBDeleteRestoImageButtonQuestion(e, this)
                    }.bind(res.id));

                    image = document.createElement('img');
                    image.setAttribute('src', appImagePath + res.path);
                    image.style.cssText = "width:auto; height:auto; max-width:inherit; max-height:inherit;";

                    container.appendChild(image);
                    uploadedImagesLayer.appendChild(container);
                    // There I need to put code for render new preview IMAGE
                }
            }

        }


    }

    function RBDeleteRestoImageButtonQuestion(e,id){

        var node= e.target;
        if(node.tagName!='DIV'){
            node=node.parentNode;
        }

        var ident = node.getAttribute('id');
        var lident = ident+'di';
        //var date=new Date();
        //var time=date.getTime();

        check = document.getElementById(lident);
        if(check==null) {

            node.removeEventListener('click',function(e){RBDeleteRestoImageButtonQuestion(e,this)}.bind(id),false);
            var layer = document.createElement('div');
            layer.style.cssText = 'position:absolute; top:0px; left:0px; z-index:1000';
            layer.setAttribute('id', lident);

            var btnDelete = document.createElement('button');
            btnDelete.className = 'cine_fn_color_btn';
            btnDelete.textContent = 'Delete';
            var btnCancel = document.createElement('button');
            btnCancel.className = 'cine_fn_color_btn';
            btnCancel.textContent = 'Cancel';

            layer.appendChild(btnDelete);
            layer.appendChild(btnCancel);

            node.appendChild(layer);

            var proceedDelete = function (ev) {
                RBDeleteRestoImageQuery(ev, id);
            };

            var removeFrameLayer = function (ev) {

                btnDelete.removeEventListener('click',proceedDelete,false);
                btnCancel.removeEventListener('click',removeFrameLayer,false);

                var element = document.getElementById(lident);
                element.parentNode.removeChild(element);

            };

            btnDelete.addEventListener('click', proceedDelete);
            btnCancel.addEventListener('click', removeFrameLayer);

        }

    }

    function RBDeleteRestoImageButtonRemove(e,id){
        var ident = e.getAttribute('id');
        var lident=ident+'di';
        check = document.getElementById(lident);
        if(check!=null) {
            check.parentNode.removeChild(check);
        }
    }

    function RBDeleteRestoImageQuery(e,id){

        XWF.connQueryObject = {query:"admindata", type:"putrestoimgdel"};
        XWF.connQueryData = {ident:RB.lastOpenedRestaurant,data:id};

        $cine().userkey(XWF.connQueryData);

        XWF.send({
            waitCallback:true,
            onComplete:RBDeleteRestoImageAnswer
        });

    }

    function RBDeleteRestoImageAnswer(data){

        var obj=data.data;

        if(obj.success){

            image=document.getElementById('restoimage'+obj.result.id);
            console.log('restoimage'+obj.result.id);
            console.log(image);
            if(image!=null){
                image.parentNode.removeChild(image);
            }

        }

    }

    // *********************** DETAILS AND IMAGES ************************
    // END





    // -------------------------------------------------------------------
    //
    //      RESTAURANT SUBMERCHANT EDIT AND CREATION
    //
    // -------------------------------------------------------------------

    function cTestSubmerchant(i){

        XWF.connQueryObject = {query:"admindata", type:"findsubmerchant"};
        XWF.connQueryData = {ident:i,user:0,data:""};

        $cine().userkey(XWF.connQueryData);

        XWF.send({
            waitCallback:true,
            onComplete:cTestSubmerchantShow
        });

    }

    function cTestSubmerchantShow(data){

        var e=RB.infoPane;
        var obj=data.data;

        var o,b,c,vf,lf;
        o=document.createElement('div');
        o.style.cssText='position:absolute; width:100%; background:#f8f8f8; left:0; top:0; height:100%;';
        o.setAttribute('id','merchant_data_layer');

        c=document.createElement('div');
        c.style.cssText='padding-right:25px; width:100%; height:50px;';
        b=document.createElement('button');
        o.style.textAlign='left';
        b.className="cine_fn_color_btn cine_short_btn";
        b.style.cssText='float:right; width:96px !important; margin-right:25px;';
        b.innerHTML='Close';
        b.addEventListener('click',function(e){this.parentNode.removeChild(this);}.bind(o));
        c.appendChild(b);
        o.appendChild(c);

        c=document.createElement('div');
        c.setAttribute('id','merchant_data');
        c.style.cssText='padding-right:25px; width:100%; height:auto; text-align:center;';

        lf=document.createElement('div');
        lf.innerHTML='<h2>Query details</h2>';
        lf.style.cssText = 'border-bottom:1px dotted #666666; margin-top:15px; margin-bottom:15px;';
        c.appendChild(lf);


        if(obj.success){
            var r=obj.result;
            if(r)
            {
                var i, k, cur, t, tr, td;
                t=document.createElement('table');
                r=r['_attributes'];

                for(k in r){

                    if(r.hasOwnProperty(k))
                    {
                        cur = r[k];
                        if (k == 'id' || k == 'status') {
                            tr = document.createElement('tr');
                            td = document.createElement('td');
                            td.innerHTML = k;
                            tr.appendChild(td);
                            td = document.createElement('td');
                            td.innerHTML = cur;
                            tr.appendChild(td);
                            t.appendChild(tr);
                        }
                        if (k == 'individual' || k == 'business' || k == 'funding') {

                            for (i in cur) {

                                if (typeof cur[i] !== 'object') {
                                    tr = document.createElement('tr');
                                    td = document.createElement('td');
                                    td.innerHTML = i;
                                    tr.appendChild(td);
                                    td = document.createElement('td');
                                    td.innerHTML = cur[i];
                                    tr.appendChild(td);
                                    t.appendChild(tr);
                                }

                            }

                        }
                    }

                }

                c.appendChild(t);
            }

        } else {

            lf=document.createElement('div');
            lf.innerHTML='<h4> Query failed due to inexistance of submerchant </h4>';
            lf.style.cssText = 'border-bottom:1px dotted #666666; margin-top:15px; margin-bottom:15px;';
            c.appendChild(lf);

        }

        o.appendChild(c);
        e.appendChild(o);
    }

    function cEditSubmerchant(i){

        var o,b,c,vf,lf;
        var e=RB.infoPane;

        o=document.createElement('div');
        o.style.cssText='position:absolute; width:100%; background:#f8f8f8; left:0; top:0;';
        o.setAttribute('id','merchant_data_layer');

        c=document.createElement('div');
        c.style.cssText='padding-right:25px; width:100%; height:50px;';
        b=document.createElement('button');
        o.style.textAlign='left';
        b.className="cine_fn_color_btn cine_short_btn";
        b.style.cssText='float:right; width:96px !important; margin-right:25px;';
        b.innerHTML='Close';
        b.addEventListener('click',function(e){this.parentNode.removeChild(this);}.bind(o));
        c.appendChild(b);
        o.appendChild(c);


        c=document.createElement('div');
        c.setAttribute('id','merchant_data');
        c.style.cssText='padding-right:25px; width:100%; height:auto; text-align:center;';

        lf=document.createElement('div');
        lf.innerHTML='<h4>Responsible person details</h4>';
        lf.style.cssText = 'border-bottom:1px dotted #666666; margin-top:15px; margin-bottom:15px;';
        c.appendChild(lf);

        // FIRST NAME
        lf=document.createElement('div');
        lf.innerHTML='First Name';
        vf=document.createElement('input');
        vf.setAttribute('id','firstname');
        vf.style.cssText='width:320px; height:32px; border:none; font-size:20px;';

        c.appendChild(lf);
        c.appendChild(vf);

        // LAST NAME
        lf=document.createElement('div');
        lf.innerHTML='Last Name';
        vf=document.createElement('input');
        vf.setAttribute('id','lastname');
        vf.style.cssText='width:320px; height:32px; border:none; font-size:20px;';

        c.appendChild(lf);
        c.appendChild(vf);

        // EMAIL USER
        lf=document.createElement('div');
        lf.innerHTML='Email';
        vf=document.createElement('input');
        vf.setAttribute('id','user_email');
        vf.style.cssText='width:320px; height:32px; border:none; font-size:20px;';

        c.appendChild(lf);
        c.appendChild(vf);

        // PHONE USER
        lf=document.createElement('div');
        lf.innerHTML='Phone (ex. 5556667788)';
        vf=document.createElement('input');
        vf.setAttribute('id','user_phone');
        vf.style.cssText='width:320px; height:32px; border:none; font-size:20px;';

        c.appendChild(lf);
        c.appendChild(vf);

        // DATE OF BIRTH USER
        lf=document.createElement('div');
        lf.innerHTML='Date of Birth (ex. 1961-04-12)';
        vf=document.createElement('input');
        vf.setAttribute('id','user_birth');
        vf.style.cssText='width:320px; height:32px; border:none; font-size:20px;';

        c.appendChild(lf);
        c.appendChild(vf);

        // SOCIAL SECURITY
        lf=document.createElement('div');
        lf.innerHTML='SSN NUMBER (left blank for auto processing)';
        vf=document.createElement('input');
        vf.setAttribute('id','user_ssn');
        vf.style.cssText='width:320px; height:32px; border:none; font-size:20px;';

        c.appendChild(lf);
        c.appendChild(vf);

        // USER STREET
        lf=document.createElement('div');
        lf.innerHTML='Street address';
        vf=document.createElement('input');
        vf.setAttribute('id','user_street');
        vf.style.cssText='width:320px; height:32px; border:none; font-size:20px;';

        c.appendChild(lf);
        c.appendChild(vf);

        // USER LOCALITY
        lf=document.createElement('div');
        lf.innerHTML='Locality';
        vf=document.createElement('input');
        vf.setAttribute('id','user_locality');
        vf.style.cssText='width:320px; height:32px; border:none; font-size:20px;';

        c.appendChild(lf);
        c.appendChild(vf);

        // USER REGION
        lf=document.createElement('div');
        lf.innerHTML='Region';
        vf=document.createElement('input');
        vf.setAttribute('id','user_region');
        vf.style.cssText='width:320px; height:32px; border:none; font-size:20px;';

        c.appendChild(lf);
        c.appendChild(vf);

        // USER POSTAL CODE
        lf=document.createElement('div');
        lf.innerHTML='Postal code (ZIP)';
        vf=document.createElement('input');
        vf.setAttribute('id','user_zip');
        vf.style.cssText='width:320px; height:32px; border:none; font-size:20px;';

        c.appendChild(lf);
        c.appendChild(vf);

        lf=document.createElement('div');
        lf.innerHTML='<h4>Business merchant account details</h4>';
        lf.style.cssText = 'border-bottom:1px dotted #666666; margin-top:15px; margin-bottom:15px;';
        c.appendChild(lf);

        // LEGAL NAME
        lf=document.createElement('div');
        lf.innerHTML='Legal Name';
        vf=document.createElement('input');
        vf.setAttribute('id','legalname');
        vf.style.cssText='width:320px; height:32px; border:none; font-size:20px;';

        c.appendChild(lf);
        c.appendChild(vf);
        // DBA NAME
        lf=document.createElement('div');
        lf.innerHTML='DBA Name';
        vf=document.createElement('input');
        vf.setAttribute('id','dbaname');
        vf.style.cssText='width:320px; height:32px; border:none; font-size:20px;';

        c.appendChild(lf);
        c.appendChild(vf);
        // TAX ID
        lf=document.createElement('div');
        lf.innerHTML='TAX Identificator (XX-XXXXXXX)';
        vf=document.createElement('input');
        vf.setAttribute('id','taxid');
        vf.style.cssText='width:320px; height:32px; border:none; font-size:20px;';

        c.appendChild(lf);
        c.appendChild(vf);
        // ADDRESS_STREET
        lf=document.createElement('div');
        lf.innerHTML='Street Address (ex. 111 Main St)';
        vf=document.createElement('input');
        vf.setAttribute('id','address_street');
        vf.style.cssText='width:320px; height:32px; border:none; font-size:20px;';

        c.appendChild(lf);
        c.appendChild(vf);
        // ADDRESS_LOCALITY
        lf=document.createElement('div');
        lf.innerHTML='Address locality (ex. New York, Chicago, etc.)';
        vf=document.createElement('input');
        vf.setAttribute('id','address_locality');
        vf.style.cssText='width:320px; height:32px; border:none; font-size:20px;';

        c.appendChild(lf);
        c.appendChild(vf);
        // ADDRESS_LOCALITY
        lf=document.createElement('div');
        lf.innerHTML='Address region (ex. NY, IL, OR, etc.)';
        vf=document.createElement('input');
        vf.setAttribute('id','address_region');
        vf.style.cssText='width:320px; height:32px; border:none; font-size:20px;';

        c.appendChild(lf);
        c.appendChild(vf);
        // ADDRESS_ZIP
        lf=document.createElement('div');
        lf.innerHTML='Postal code';
        vf=document.createElement('input');
        vf.setAttribute('id','address_zip');
        vf.style.cssText='width:320px; height:32px; border:none; font-size:20px;';

        c.appendChild(lf);
        c.appendChild(vf);



        lf=document.createElement('div');
        lf.innerHTML='<h4>Funding account details</h4>';
        lf.style.cssText = 'border-bottom:1px dotted #666666; margin-top:15px; margin-bottom:15px;';
        c.appendChild(lf);

        // DESCRIPTOR
        //lf=document.createElement('div');
        //lf.innerHTML='Descriptor name (as Legal Name or DBA Name)';
        //vf=document.createElement('input');
        //vf.setAttribute('id','descriptor');
        //vf.style.cssText='width:320px; height:32px; border:none; font-size:20px;';

        //c.appendChild(lf);
        //c.appendChild(vf);

        // DESTINATION
        lf=document.createElement('div');
        lf.innerHTML='Destination (Phone, Email, Bank)';
        vf=document.createElement('input');
        vf.setAttribute('id','destination');
        vf.style.cssText='width:320px; height:32px; border:none; font-size:20px;';
        vf.setAttribute('value','Bank');

        c.appendChild(lf);
        c.appendChild(vf);
        // EMAIL
        lf=document.createElement('div');
        lf.innerHTML='Email';
        vf=document.createElement('input');
        vf.setAttribute('id','email');
        vf.style.cssText='width:320px; height:32px; border:none; font-size:20px;';

        c.appendChild(lf);
        c.appendChild(vf);
        // MOBILE PHONE
        lf=document.createElement('div');
        lf.innerHTML='Mobile phone';
        vf=document.createElement('input');
        vf.setAttribute('id','mobile');
        vf.style.cssText='width:320px; height:32px; border:none; font-size:20px;';

        c.appendChild(lf);
        c.appendChild(vf);
        // ACCOUNT NUMBER
        lf=document.createElement('div');
        lf.innerHTML='Account Number';
        vf=document.createElement('input');
        vf.setAttribute('id','account');
        vf.style.cssText='width:320px; height:32px; border:none; font-size:20px;';

        c.appendChild(lf);
        c.appendChild(vf);
        // ROUTING NUMBER
        lf=document.createElement('div');
        lf.innerHTML='Routing Number';
        vf=document.createElement('input');
        vf.setAttribute('id','route');
        vf.style.cssText='width:320px; height:32px; border:none; font-size:20px;';

        c.appendChild(lf);
        c.appendChild(vf);
        o.appendChild(c);

        var f=c;
        c=document.createElement('div');
        c.style.cssText='width:100%; height:auto; margin:20px 0 20px 0; text-align:center;';
        b=document.createElement('button');
        o.style.textAlign='left';
        b.className="cine_fn_color_btn cine_short_btn";
        b.style.cssText='width:256px !important;';
        b.innerHTML='Create Submerchant Account';
        b.addEventListener('click',function(){cSubMerchantCreate(this.el,this.id);}.bind({el:f,id:i}));
        c.appendChild(b);

        o.appendChild(c);
        e.appendChild(o);

    }


    function cSubMerchantCreate(o,rid){

        var n=o.childNodes;
        var l=n.length;
        var i,e;
        var r={};
        for(i=0;i<l;i++){
            e=n[i];
            if(e.nodeName=='INPUT'){
                r[e.id]= e.value;
            }
        }
        r=JSON.stringify(r);

        XWF.connQueryObject = {query:"admindata", type:"putsubmerchant"};
        XWF.connQueryData = {ident:rid,user:0,data:r};

        $cine().userkey(XWF.connQueryData);

        XWF.send({
            waitCallback:true,
            onComplete:procSubMerchantClose
        })
    }

    function procSubMerchantClose(data){

        var obj=data.data;

        if(obj.success){

            if(obj.result){

                alert('Operation successfull: Your request for submerchant is pending, wait for results refreshing the Restaurant pane');

            } else {


            }

        }

    }


    if (!window.RB) {
        window.RB = $cr();
    }

})();