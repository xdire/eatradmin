/**
 * Created by xdire on 18.03.15.
 */

(function() {

    var $locu = function () {
        return new LOCU();
    };

    var LOCU = function () {

        this.searchField=null;
        this.searchCity=null;
        this.searchZip=null;

        this.resultsField=null;
        this.resultsPage=0;

        this.restaurantList=null;
        this.restaurantMenu=null;
        this.menuObject=null;

        this.open = function (e) {

            LUopenBuilder(e);

        };

        function LUdoSearch(){

            var name=LOCU.searchField.value;
            var city=LOCU.searchCity.value;
            var zip=LOCU.searchZip.value;

            if(typeof page==='undefined'){
                page=0;
            }

            XWF.connQueryObject = {query:"bulkdata",type:"getrestolocu"};
            XWF.connQueryData = {data:name,city:city,zip:zip,page:page};
            XWF.connEarlyTermination=false;

            var data=XWF.connQueryData;

            if(city.length>0 || zip.length>0 || name.length>0)
            {
                XWF.send({
                    waitCallback: true, onComplete: LUoutputLocations, data: function () {return $cine().userkeyupd(data);}
                });
            }
            else {
                alert('Some search values are empty');
            }

        }

        function LUoutputLocations(data){

            LOCU.restaurantList={};
            LOCU.restaurantMenu={};
            LOCU.menuObject={};

            var obj=data.data;
            var num=0,cur,menu,location,contact,tr,td;

            var content=document.createElement('div');
            var table=document.createElement('table');
            table.className='table_bulk';

            if(obj.success)
            {

                result=obj.result;
                length=result.length;

                if(obj.existed)
                    existed=obj.existed;
                else
                    existed={};

                for(var i=0;i<length;i++){

                    cur=result[i];
                    if(cur.hasOwnProperty('menus')){

                        var restoItem=
                        {
                            name:cur['name'],
                            location:{
                                address1:null,
                                region:null,
                                city:null,
                                postcode:null,
                                latitude:null,
                                longitude:null
                            },
                            phones:{
                                main:null,
                                fax:null
                            },
                            description:null
                        };

                        // Create --- ROW ---
                        tr = document.createElement('tr');
                        tr.style.heigh = '64px';
                        tr.setAttribute('id', 'resto_'+cur['locu_id']);
                        if(existed[cur['locu_id']]){
                            tr.className='table_active_elem';
                        }
                        // Location Iterations
                        if(cur.hasOwnProperty('location'))
                        {

                            location=cur.location;

                            if(location.hasOwnProperty('address1')) {
                                restoItem['location']['address1'] = location['address1'];
                            } else {restoItem['location']['address1']=null}

                            if(location.hasOwnProperty('region')) {
                                restoItem['location']['region'] = location['region'];
                            } else {restoItem['location']['region']=null}

                            if(location.hasOwnProperty('locality')) {
                                restoItem['location']['city'] = location['locality'];
                            } else {restoItem['location']['city']=null}

                            if(location.hasOwnProperty('postal_code')) {
                                restoItem['location']['postcode'] = location['postal_code'];
                            } else {restoItem['location']['postcode']=null}

                            if(location.hasOwnProperty('geo')) {
                                geo=location.geo;
                                if(geo.hasOwnProperty('coordinates')){
                                    coord=geo.coordinates;
                                    restoItem['location']['latitude'] = coord[1];
                                    restoItem['location']['longitude'] = coord[0];
                                } else {
                                    restoItem['location']['latitude']=null;
                                    restoItem['location']['longitude']=null;
                                }
                            } else {
                                restoItem['location']['latitude']=null;
                                restoItem['location']['longitude']=null;
                            }

                        }

                        if(cur.hasOwnProperty('contact'))
                        {
                            contact=cur.contact;
                            if(contact.hasOwnProperty('phone'))
                            {
                                restoItem['phones']['main']=contact['phone'];
                            }
                        }


                        td = tr.insertCell(0);
                        td.textContent = cur['name'];
                        td.style.width = '15%';

                        td = tr.insertCell(1);
                        td.style.cssText='word-wrap:break-word; width:10% !important; max-width:64px;';
                        if(location.hasOwnProperty('address1')) {
                            td.textContent = restoItem.location['address1'];
                        }


                        td = tr.insertCell(2);
                        td.style.width = '10%';
                        if(location.hasOwnProperty('postal_code')) {
                            td.textContent = restoItem.location['postcode'];
                        }

                        td = tr.insertCell(3);
                        td.textContent = restoItem.location['latitude'];
                        td.style.width = '5%';

                        td = tr.insertCell(4);
                        td.textContent = restoItem.location['longitude'];
                        td.style.width = '5%';

                        td = tr.insertCell(5);
                        td.textContent = restoItem.phones['main'];
                        td.style.width = '10%';

                        btn1 = document.createElement('button');
                        btn1.className = 'cine_fn_color_btn';
                        btn1.style.width = '128px';
                        btn1.textContent = 'Grab It';
                        btn1.addEventListener('click', function (e) {

                            LUGrabRestaurant(this);

                        }.bind(cur['locu_id']));
                        td = tr.insertCell(6);
                        td.appendChild(btn1);
                        td.style.width = '20%';

                        btn2 = document.createElement('button');
                        btn2.className = 'cine_fn_color_btn';
                        btn2.style.width = '128px';
                        btn2.textContent = 'Grab Menu';
                        btn2.addEventListener('click', function (e) {

                            LUGrabMenuForLocation(this);

                        }.bind(cur['locu_id']));
                        td = tr.insertCell(7);
                        td.appendChild(btn2);
                        td.style.width = '20%';

                        td = tr.insertCell(8);
                        td.setAttribute('id','menu_'+cur['locu_id']);
                        td.innerHTML='<div style="width:24px; height:24px; display:inline-block; background-color:yellow;"></div>';
                        td.style.width = '5%';

                        table.appendChild(tr);

                        LOCU.restaurantList[cur['locu_id']]=restoItem;
                        LOCU.restaurantMenu[cur['locu_id']]=cur['menus'];

                    }

                }

            }

            content.appendChild(table);
            LOCU.resultsField.appendChild(content);

        }

        function LUGrabRestaurant(id){

            if(LOCU.restaurantList.hasOwnProperty(id)){

                var email=document.getElementById('grab_email');

                if(email!=null) {

                    XWF.connQueryObject = {query: "bulkdata", type: "putlocuresto"};
                    XWF.connQueryData = {
                        email:email.value,
                        data: id,
                        menu: JSON.stringify(LOCU.menuObject[id]),
                        restaurant: JSON.stringify(LOCU.restaurantList[id])
                    };
                    XWF.connEarlyTermination = false;

                    var data = XWF.connQueryData;

                    XWF.send({
                        waitCallback: true,
                        onComplete: LUGrabRestaurantFinish,
                        data: function () {
                            return $cine().userkeyupd(data);
                        }
                    });

                }
                else
                {
                    alert('Form error');
                }

            } else {
                alert('That restaurant is not listed in restaurant object');
            }

        }

        function LUGrabRestaurantFinish(data){

            var obj=data.data;
            console.log(obj);
            if(obj.success){

                var row=document.getElementById('resto_'+obj.result);
                if(row!=null)
                    row.className='table_active_elem';

            }

        }

        function LUGrabMenuForLocation(id){

            if(LOCU.restaurantMenu.hasOwnProperty(id)){

                win=XWF.openwindow({
                    navigationBar:true,
                    raiseWindow:true,
                    changeTitle:'Grab Restaurant Menu',
                    windowSubTitle:"Menu with list of items"
                });

                var menu=LOCU.restaurantMenu[id],
                    menuObj,
                    menuSection,
                    menuSubSection,
                    menuItem;

                var len=menu.length;

                var cont=document.createElement('div');
                cont.className='bulk-menu-list';
                cont.setAttribute('id','bulk-menu-list');

                for(var i=0;i<len;i++){
                    // TOP SECTION OF MENU
                    // -------------------
                    menuObj=menu[i];
                    menuName=menuObj.menu_name;

                    var item = document.createElement('div');
                    item.className = 'bulk-menu-menu';
                    // title
                    var title = document.createElement('input');
                    title.setAttribute('value',menuName);
                    title.className = 'item-title';
                    // description
                    var desc = document.createElement('input');
                    desc.className = 'item-desc';
                    desc.setAttribute('value', "");

                    item.appendChild(title);
                    item.appendChild(desc);

                    del = document.createElement('div');
                    del.className = 'item-delete';
                    del.textContent='Del';
                    del.addEventListener('click',function(e){

                        var parent= e.target.parentNode;
                        if(parent!=null){
                            parent.parentNode.removeChild(parent);
                        }

                    });
                    item.appendChild(del);

                    cont.appendChild(item);

                    if(menuObj.hasOwnProperty('sections')){

                        // SECOND LEVEL OF MENU
                        // --------------------
                        menuSect=menuObj.sections;
                        mslen=menuSect.length;

                        for(var is=0;is<mslen;is++){

                            menuSection=menuSect[is];
                            sectionName=menuSection.section_name;

                            item = document.createElement('div');
                            item.className = 'bulk-menu-section';
                            item.style.backgroundColor='limegreen';

                            // title
                            title = document.createElement('input');
                            title.setAttribute('value', sectionName);
                            title.className = 'item-title';
                            title.style.cssText='width:90%;';
                            item.className = 'bulk-menu-section';
                            item.appendChild(title);
                            // description
                            var sectiondesc = document.createElement('textarea');
                            sectiondesc.className = 'item-desc';
                            sectiondesc.style.cssText='height:64px; width:90%;';
                            sectiondesc.textContent = '';
                            item.appendChild(sectiondesc);
                            // delete
                            del = document.createElement('div');
                            del.className = 'item-delete';
                            del.textContent='Del';
                            del.addEventListener('click',function(e){

                                var parent= e.target.parentNode;
                                if(parent!=null){
                                    parent.parentNode.removeChild(parent);
                                }

                            });
                            item.appendChild(del);

                            cont.appendChild(item);

                            if(menuSection.hasOwnProperty('subsections')){

                                // SUBSECTION LEVEL OF SECOND LEVEL OF MENU
                                // ----------------------------------------
                                menuSubSect=menuSection.subsections;
                                msslen=menuSubSect.length;

                                for(var iss=0;iss<msslen;iss++){

                                    menuSubSection=menuSubSect[iss];

                                    if(menuSubSection.hasOwnProperty('contents')){

                                        // ITEMS LEVEL OF MENU, OR NOT ONLY ITEMS
                                        // --------------------------------------
                                        menuContents=menuSubSection.contents;
                                        mclen=menuContents.length;

                                        for(var imc=0;imc<mclen;imc++){

                                            menuItem=menuContents[imc];

                                            if(menuItem.type=='ITEM'){

                                                item = document.createElement('div');
                                                item.className = 'bulk-menu-item';

                                                // title
                                                title = document.createElement('input');
                                                title.setAttribute('value', menuItem.name);
                                                title.className = 'item-title';

                                                item.appendChild(title);
                                                // price
                                                var price = document.createElement('input');
                                                price.className = 'item-price';

                                                if (menuItem.hasOwnProperty('price'))
                                                {
                                                    numprice=Number(menuItem.price);
                                                    if(!isNaN(numprice)) {
                                                        price.setAttribute('value', numprice);
                                                    }
                                                }

                                                item.appendChild(price);
                                                // description
                                                desc = document.createElement('input');
                                                desc.className = 'item-desc';

                                                if (menuItem.hasOwnProperty('description')) {
                                                    desc.setAttribute('value', menuItem.description);
                                                }

                                                item.appendChild(desc);
                                                // delete
                                                del = document.createElement('div');
                                                del.className = 'item-delete';
                                                del.textContent='Del';
                                                del.addEventListener('click',function(e){

                                                    var parent= e.target.parentNode;
                                                    if(parent!=null){
                                                        parent.parentNode.removeChild(parent);
                                                    }

                                                });
                                                item.appendChild(del);

                                                cont.appendChild(item);

                                            }
                                            else if(menuItem.type=='SECTION_TEXT'){
                                                // section description
                                                existedDesc=sectiondesc.value;
                                                sectiondesc.textContent=existedDesc+' '+menuItem.text;
                                            }

                                        }

                                    }

                                }

                            }

                        }

                    }

                }

                // AGREE WITH RENDERED MENU
                var block=document.createElement('div');
                var buttonAgree=document.createElement('button');
                buttonAgree.className = 'cine_fn_color_btn';
                buttonAgree.style.width = '400px';
                buttonAgree.textContent = 'Agree with structure and apply menu';
                buttonAgree.addEventListener('click', function (e) {

                    LUGrabMenuStructureAgree(this);

                }.bind({location:id,object:cont}));
                block.appendChild(buttonAgree);
                cont.appendChild(block);

                win.content.appendChild(cont);

            } else {
                alert('No menu presented for that restaurant');
            }

        }

        function LUGrabMenuStructureAgree(obj)
        {

            var object=obj.object;
            var location=obj.location;

            var menuObject={};
            var menuTop=null;
            var menuSection=null;

            var childs=object.childNodes;
            var length=childs.length;
            var i=0;

            for(i=0;i<length;i++){

                var child=childs[i];

                if(child.className=='bulk-menu-menu'){
                    subChild=child.childNodes;
                    title=SPjsonUnescapeQuotes(subChild[0].value);
                    desc=SPjsonUnescapeQuotes(subChild[1].value);
                    date=new Date();
                    newId='n'+(date.getTime()+i);
                    menuObject[newId]={sta:true,title:title,desc:desc,bypass:false,price:null,image:"",bcolor:"",props:"",include:false};
                    menuTop=menuObject[newId];
                }
                else if(child.className=='bulk-menu-section'){

                    if(menuTop!=null) {

                        subChild=child.childNodes;
                        title=SPjsonUnescapeQuotes(subChild[0].value);
                        desc=SPjsonUnescapeQuotes(subChild[1].value);

                        date = new Date();
                        newId = 'n' + (date.getTime() + i);

                        if (!menuTop.include) {
                            menuTop.include = {};
                        }

                        menuTop.include[newId] = {
                            sta:true,title: title, desc: desc,
                            price: null, image: "",
                            bcolor: "", props: "",allr:"",cal:"",
                            include: false
                        };

                        menuSection = menuTop.include[newId];
                    }
                }
                else if(child.className=='bulk-menu-item'){

                    if(menuSection!=null){

                        subChild=child.childNodes;
                        title=SPjsonUnescapeQuotes(subChild[0].value);
                        price=Number(SPjsonUnescapeQuotes(subChild[1].value));
                        desc=SPjsonUnescapeQuotes(subChild[2].value);

                        if(price == 0 || isNaN(price)){
                            price=-1;
                        }

                        date = new Date();
                        newId = 'n' + (date.getTime() + i);

                        if (!menuSection.include) {
                            menuSection.include = {};
                        }

                        menuSection.include[newId] = {
                            sta:true,title: title, desc: desc,
                            price: price, image: "",
                            bcolor: "", props: "",allr:"",cal:"",
                            include: false
                        };

                    }

                }

            }

            if(i>0)
            {
                LOCU.menuObject[location]=menuObject;
                var menuColor=document.getElementById('menu_'+location);
                if(menuColor!=null){
                    menuColor.innerHTML='<div style="width:24px; height:24px; display:inline-block; background-color:limegreen;"></div>';
                }
                XWF.closewindow();

            } else {
                alert('Menu not converted');
            }

        }

        function LUopenBuilder(e){

            win=XWF.openwindow({
                navigationBar:true,
                raiseWindow:true,
                changeTitle:'LOCU API Restaurants',
                windowSubTitle:"List"
            });

            var emailBlock=document.createElement('div');
            var emailInput=document.createElement('input');
            emailInput.style.cssText='width:98%; margin:10px 1% 0 1%;';
            emailInput.setAttribute('placeholder','Type in Email of Actual User');
            emailInput.setAttribute('type','text');
            emailInput.setAttribute('id','grab_email');
            emailBlock.appendChild(emailInput);
            win.content.appendChild(emailBlock);

            var search=document.createElement('input');
            search.style.marginTop='22px';
            search.setAttribute('placeholder','Restaurant Name');
            search.setAttribute('type','text');
            search.setAttribute('id','place_name');
            search.style.width='29%';
            LOCU.searchField=search;

            win.content.appendChild(search);

            var region=document.createElement('input');
            region.style.marginTop='22px';
            region.setAttribute('placeholder','City name');
            region.setAttribute('type','text');
            region.setAttribute('id','city_name');
            region.style.width='20%';
            LOCU.searchCity=region;

            win.content.appendChild(region);

            var zip=document.createElement('input');
            zip.style.marginTop='22px';
            zip.setAttribute('placeholder','Zip code');
            zip.setAttribute('type','hidden');
            zip.setAttribute('id','zip_code');
            zip.style.width='10%';
            LOCU.searchZip=zip;

            win.content.appendChild(zip);

            var buttonSearch=document.createElement('button');
            buttonSearch.style.cssText='margin-top:20px !important';
            buttonSearch.className='cine_fn_color_btn';
            buttonSearch.style.width='39%';
            buttonSearch.textContent='Search Locations';
            buttonSearch.addEventListener('click',function(e){
                LUdoSearch(search);
            });

            win.content.appendChild(buttonSearch);

            var results=document.createElement('div');
            results.setAttribute('id','resto_results');
            LOCU.resultsField=results;
            win.append(results);

        }

    };

    if(!window.LOCU){window.LOCU = $locu();}
})();