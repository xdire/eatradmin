/**
 * Created by xdire on 23.02.15.
 */


(function() {

    var $cine = function () {
        return new MBF();
    };

    var MBF = function()
    {
        this.restaurant=null;
        this.restaurantTitle=null;

        this.menu=null;
        this.side=null;

        this.menuObject=null;
        this.sideObject=null;
        this.menuIndex=null;
        this.menuIndexLength=0;
        this.menuIndexHeader=null;

        this.privateLayer=null;
        this.privateLayerQueue=[];
        this.privateCurBlockLayer=null;

        this.privateCurItemSideLayer=null;
        this.privateCurItemSides=null;

        this.privateCurSideLayer=null;
        this.privateCurSideVarLayer=null;

        this.viewTree=false;

        this.dragObject=[];

        this.open = function(e){

            MBOpenMenuBuilder(e);

        };

        this.openTree = function(e){

            MBOpenMenuBuilderTree(e);

        };

        this.editItem = function(e,id){

            if(MB.menuIndex.hasOwnProperty(id)){

                object=MB.menuIndex[id];
                MBMenuEditItem(object);

            }

        };

        this.deleteItem = function(e,id){

            if(MB.menuIndex.hasOwnProperty(id)){

                object=MB.menuIndex[id];
                MBMenuDeleteItemQuery(object);

            }

        };

        this.addTreeElement = function(e,id){

            MBMenuAddTreeItem(e,id);

        };

        this.dragStart = function(e,id){

            MB.dragObject.push(MB.menuIndex[id]);

        };

        this.dragEnd = function(e){
            MB.dragObject=[];
        };

        this.dragOver = function(e,i){
            MBMenuDragOver(e,i);
        };

        this.dragLeave = function(e,i){
            MBMenuDragLeave(e,i);
        };

        this.dragDrop = function(e,i){
            MBMenuDragDrop(e,i);
        };

        this.editInline = function(e,id,type){
            MBMenuEditInline(e,id,type);
        };

        this.cloneItem = function(e,id){
            MBMenuCloneItem(e,id);
        }
    };

    function MBOpenMenuBuilderTree(e)
    {

        XWF.connQueryObject = {query:"admindata", type:"getrestomenu"};
        XWF.connQueryData = {ident:MB.restaurant,user:0};

        MBUserKey(XWF.connQueryData);

        XWF.send({
            navigationBar:true,
            waitCallback:true,
            raiseWindow:true,
            raiseWindowID:"menu-builder-main",
            onComplete:MBOpenMenuBuilderDisplayTree,
            changeTitle:'Menu Editor',
            windowSubTitle:"Restaurant Menu Editor"
        });
    }

    function MBOpenMenuBuilderDisplayTree(data)
    {

        MB.viewTree=true;

        var obj=data.data;
        var cont=data.content;

        MB.privateLayer=cont;
        MB.privateLayerQueue=[];

        var layer=document.createElement('div');
        layer.setAttribute('id','meditor_l1');
        var control=document.createElement('div');
        var blocks=document.createElement('div');
        blocks.className='cine_fn_resto_cpanel';
        blocks.style.paddingTop=0;

        MB.privateCurBlockLayer=blocks;

        layer.appendChild(control);
        layer.appendChild(blocks);

        var cpanel = MBMenuDrawControlPanel({root:false,subroot:false,level:0,treeview:true});
        control.appendChild(cpanel);

        cont.appendChild(layer);
        MB.privateLayerQueue.push(layer);

        if(obj.success)
        {

            if(obj.side){
                MB.sideObject=obj.side;
            }

            if(obj.result){

                var menu=obj.result;

                if(typeof menu==='object'){
                    MB.menuObject = menu;
                } else {
                    MB.menuObject = {};
                    menu={};
                }

                MBMenuCreateIndex();
                var current,index,include,current2,index2,include2,current3,index3;

                for(var k in menu){

                    if(menu.hasOwnProperty(k)){
                        // Level 1 cycle
                        current=menu[k];
                        index=MB.menuIndex[k];

                        layer1=MBMenuTreeItem(current,k,0);

                        if(index.include)
                        {

                            include=current.include;

                            for(var sk in include){

                                if(include.hasOwnProperty(sk)){
                                    // Level 2 cycle
                                    current2=include[sk];
                                    index2=MB.menuIndex[sk];

                                    layer2=MBMenuTreeItem(current2,sk,1);

                                    if(index2.include)
                                    {

                                        include2=current2.include;

                                        for(var tk in include2) {

                                            if (include2.hasOwnProperty(tk)) {
                                                // Level 3 cycle
                                                current3=include2[tk];
                                                index3=MB.menuIndex[tk];

                                                layer3=MBMenuTreeItem(current3,tk,2);
                                                layer2.appendChild(layer3);
                                                layerSep=MBMenuTreeItemSeparator(tk,2);
                                                layer2.appendChild(layerSep);

                                            }

                                        }

                                    }

                                    layer2a=document.createElement('div');
                                    layer2a.className='menu-third-add';
                                    layer2a.textContent='Add New Item';
                                    layer2a.setAttribute('onclick','MB.addTreeElement(this,"'+sk+'")');
                                    layer2.appendChild(layer2a);
                                    layer1.appendChild(layer2);
                                    layerSep=MBMenuTreeItemSeparator(sk,1);
                                    layer1.appendChild(layerSep);

                                }

                            }

                        }

                        layer1a=document.createElement('div');
                        layer1a.className='menu-second-add';
                        layer1a.textContent='Add New Category';
                        layer1a.setAttribute('onclick','MB.addTreeElement(this,"'+k+'")');
                        layer1.appendChild(layer1a);

                        blocks.appendChild(layer1);
                        layerSep=MBMenuTreeItemSeparator(k,0);
                        blocks.appendChild(layerSep);

                    }

                }

                layer0a=document.createElement('div');
                layer0a.className='menu-first-add';
                layer0a.textContent='Add New Main Menu';
                layer0a.setAttribute('onclick','MB.addTreeElement(this,null)');
                blocks.appendChild(layer0a);

            }
            else {

                MB.menuObject = {};
                MB.menuIndex = {};
                menu={};

                layer0a=document.createElement('div');
                layer0a.className='menu-first-add';
                layer0a.textContent='Add New Main Menu';
                layer0a.setAttribute('onclick','MB.addTreeElement(this,null)');
                blocks.appendChild(layer0a);

            }

        }

    }

    function MBOpenMenuBuilder(e)
    {

        XWF.connQueryObject = {query:"admindata", type:"getrestomenu"};
        XWF.connQueryData = {ident:MB.restaurant,user:0};

        MBUserKey(XWF.connQueryData);

        XWF.send({
            navigationBar:true,
            waitCallback:true,
            raiseWindow:true,
            raiseWindowID:"menu-builder-main",
            onComplete:MBOpenMenuBuilderDisplay,
            changeTitle:'Menu Editor',
            windowSubTitle:"Restaurant Menu Editor"
        });
    }

    function MBOpenMenuBuilderDisplay(data)
    {

        MB.viewTree=false;

        var obj=data.data;
        var cont=data.content;

        MB.privateLayer=cont;
        MB.privateLayerQueue=[];

        var layer=document.createElement('div');
        layer.setAttribute('id','meditor_l1');
        var control=document.createElement('div');
        var blocks=document.createElement('div');
        blocks.className='cine_fn_resto_cpanel';
        blocks.style.paddingTop=0;

        MB.privateCurBlockLayer=blocks;

        layer.appendChild(control);
        layer.appendChild(blocks);

        var cpanel = MBMenuDrawControlPanel({root: false, subroot: false,level:0});
        control.appendChild(cpanel);

        cont.appendChild(layer);
        MB.privateLayerQueue.push(layer);

        if(obj.success){

            if(obj.side){
                MB.sideObject=obj.side;
            } else {
                MB.sideObject={};
            }

            if(obj.result){

                var menu=obj.result;

                if(typeof menu==='object'){
                    MB.menuObject = menu;
                } else {
                    MB.menuObject = {};
                    menu={};
                }

                MBMenuCreateIndex();

                for(var k in menu){

                    current=menu[k];
                    element=MBMenuItem(current,k);
                    blocks.appendChild(element);

                }

            }

        }

    }

    function MBMenuDrawLevel(id){

        var obj=MBMenuFindObject(id);

        if(obj)
        {

            var meta=MB.menuIndex[id];

            if(meta.level<3){

                var inc=obj.include;

                var layer=document.createElement('div');
                layer.className='cine_fn_resto_cpanel';
                layer.style.cssText='padding-top:0;';
                layer.setAttribute('id','meditor_l'+(meta.level+1));

                var control=document.createElement('div');
                var blocks=document.createElement('div');
                blocks.className='cine_fn_resto_cpanel';
                blocks.style.cssText='padding-top:0;';

                layer.appendChild(control);
                layer.appendChild(blocks);

                MB.privateCurBlockLayer=blocks;

                var prevLayer=MB.privateLayerQueue[meta.level-1];
                prevLayer.style.display='none';

                var cpanel = MBMenuDrawControlPanel(meta);
                control.appendChild(cpanel);

                var bcrumb = MBMenuDrawBreadcrumbs(meta);
                control.appendChild(bcrumb);

                for(var k in inc){

                    current=inc[k];
                    element=MBMenuItem(current,k);
                    blocks.appendChild(element);

                }

                MB.privateLayer.appendChild(layer);
                MB.privateLayerQueue.push(layer);

            } else {

                MBMenuEditItem(meta);

            }

        }

    }

    function MBMenuDrawBreadcrumbs(obj){

        var o=document.createElement('div');
        o.className='cine_fn_bcbar';
        o.style.cssText='height:18px; font-size:12px; padding-left:13px';
        o.textContent='';
        if(obj.level>0)
        {
            var l1,l2;
            if(obj.root){
                l1=MB.menuIndex[obj.root];
                o.textContent+=l1.title + ' > ';
            }
            o.textContent+=obj.title + ' > ';
        }
        return o;

    }

    function MBMenuDrawControlPanel(obj){

        var o=document.createElement('div');
        o.className='cine_fn_cbar';

        var treeview=false;
        if(obj.hasOwnProperty('treeview')){
            treeview=true;
        }

        if(!treeview)
        {
            if (obj.level > 0) {
                //Add
                var a = document.createElement('a');
                a.className = 'cine_fn_cbar_elem';
                a.innerHTML = ' Previous ';

                a.addEventListener('click', function () {

                    var prevLayer = MB.privateLayerQueue[this - 1];
                    prevLayer.style.display = 'block';
                    MB.privateCurBlockLayer = prevLayer.lastChild;

                    var currLayer = MB.privateLayerQueue[this];
                    currLayer.parentNode.removeChild(currLayer);

                    arrayRemove(MB.privateLayerQueue, this);

                }.bind(obj.level));

                o.appendChild(a);

            }

            if (obj.level < 3) {
                //Add
                var a = document.createElement('a');
                a.className = 'cine_fn_cbar_elem';
                a.innerHTML = ' Add ';
                a.addEventListener('click', function (e) {

                    MBMenuAddItem(this);

                }.bind(obj));
                o.appendChild(a);
            }

            //Edit
            a = document.createElement('a');
            a.className = 'cine_fn_cbar_elem';
            a.textContent = ' Edit ';
            a.addEventListener('click', function () {
            });
            o.appendChild(a);
        }

        //Save
        a=document.createElement('a');
        a.className='cine_fn_cbar_elem_r';
        a.textContent=' Save Menu ';
        a.addEventListener('click',function(e){MBSaveMenu(e)});
        o.appendChild(a);
        //Sides
        a=document.createElement('a');
        a.className='cine_fn_cbar_elem_r';
        a.textContent=' Open Sides ';
        a.addEventListener('click',function(e){MBOpenMenuSideBuilder(e)});
        o.appendChild(a);

        return o;

    }

    function MBMenuAddTreeItem(eventTarget,id)
    {

        var meta = MB.menuIndex[id];
        if(id==null || meta==null){
            meta={root:false,level:0};
        }

        var win=cOpenTinyModal({appTitle:"Add Menu Item"});

        var control=document.createElement('div');
        var button=document.createElement('div');
        control.style.cssText='position:relative; height:85%; overflow-y:scroll; width:100%;';
        button.style.cssText='position:relative; height:15%; overflow:hidden; width:100%;';

        win.content.appendChild(control);
        win.content.appendChild(button);

        saveBtn=document.createElement('button');
        saveBtn.className='cine_fn_color_btn';
        saveBtn.textContent='Save';
        cancelBtn=document.createElement('button');
        cancelBtn.className='cine_fn_color_btn';
        cancelBtn.textContent='Cancel';
        cancelBtn.addEventListener('click',function(e){cRemoveTinyModal()});

        button.appendChild(saveBtn);
        button.appendChild(cancelBtn);

        specialMeta=clone(meta);
        specialMeta.level=specialMeta.level+1;
        control.appendChild(MBMenuAddItemHTML(specialMeta));

        if(meta.root)
        {
            saveBtn.addEventListener('click',function(e){
                title=document.getElementById('cmitem_title');
                desc=document.getElementById('cmitem_desc');
                price=document.getElementById('cmitem_price');
                image=document.getElementById('cmitem_image');
                MBMenuAddItemSaveItem(meta,{sta:true,title:title.value,desc:desc.value,price:price.value,image:image.value,allr:null,cal:null},eventTarget);
            });
        }
        else
        {
            saveBtn.addEventListener('click',function(e){
                title=document.getElementById('cmitem_title');
                desc=document.getElementById('cmitem_desc');
                price=document.getElementById('cmitem_price');
                image=document.getElementById('cmitem_image');
                MBMenuAddItemSaveItem(meta,{sta:true,title:title.value,desc:desc.value,price:price.value,image:image.value,allr:null,cal:null},eventTarget);
            });
        }

        title=document.getElementById('cmitem_title');
        title.focus();
        title.selectionStart = title.selectionEnd = title.value.length;

    }

    function MBMenuAddItem(object){

        var win=cOpenTinyModal({appTitle:"Add Menu Item"});

        var control=document.createElement('div');
        var button=document.createElement('div');
        control.style.cssText='position:relative; height:85%; overflow-y:scroll; width:100%;';
        button.style.cssText='position:relative; height:15%; overflow:hidden; width:100%;';

        win.content.appendChild(control);
        win.content.appendChild(button);

        saveBtn=document.createElement('button');
        saveBtn.className='cine_fn_color_btn';
        saveBtn.textContent='Save';
        cancelBtn=document.createElement('button');
        cancelBtn.className='cine_fn_color_btn';
        cancelBtn.textContent='Cancel';
        cancelBtn.addEventListener('click',function(e){cRemoveTinyModal()});

        button.appendChild(saveBtn);
        button.appendChild(cancelBtn);

        control.appendChild(MBMenuAddItemHTML(object));

        if(object.root)
        {
            saveBtn.addEventListener('click',function(e){
                title=document.getElementById('cmitem_title');
                desc=document.getElementById('cmitem_desc');
                price=document.getElementById('cmitem_price');
                image=document.getElementById('cmitem_image');
                MBMenuAddItemSaveItem(object,{sta:true,title:title.value,desc:desc.value,price:price.value,image:image.value,allr:null,cal:null});
            });
        }
        else
        {
            saveBtn.addEventListener('click',function(e){
                title=document.getElementById('cmitem_title');
                desc=document.getElementById('cmitem_desc');
                price=document.getElementById('cmitem_price');
                image=document.getElementById('cmitem_image');
                MBMenuAddItemSaveItem(object,{sta:true,title:title.value,desc:desc.value,price:price.value,image:image.value,allr:null,cal:null});
            });
        }

        title=document.getElementById('cmitem_title');
        title.focus();
        title.selectionStart = title.selectionEnd = title.value.length;

    }

    function MBMenuEditItem(object){

        var win=cOpenTinyModal({appTitle:"Edit Menu Item"});

        var control=document.createElement('div');
        var button=document.createElement('div');
        control.style.cssText='position:relative; height:85%; overflow-y:scroll; width:100%;';
        button.style.cssText='position:relative; height:15%; overflow:hidden; width:100%;';

        win.content.appendChild(control);
        win.content.appendChild(button);

        saveBtn=document.createElement('button');
        saveBtn.className='cine_fn_color_btn';
        saveBtn.textContent='Save';
        cancelBtn=document.createElement('button');
        cancelBtn.className='cine_fn_color_btn';
        cancelBtn.textContent='Cancel';
        cancelBtn.addEventListener('click',function(e){cRemoveTinyModal()});

        button.appendChild(saveBtn);
        button.appendChild(cancelBtn);

        data=MBMenuFindObject(object.id);
        control.appendChild(MBMenuAddItemHTML(object,data));

        if(object.root)
        {
            saveBtn.addEventListener('click',function(e){
                title=document.getElementById('cmitem_title');
                desc=document.getElementById('cmitem_desc');
                price=document.getElementById('cmitem_price');
                image=document.getElementById('cmitem_image');
                MBMenuAddItemUpdateItem(object,{sta:true,title:title.value,desc:desc.value,price:price.value,image:image.value,allr:null,cal:null});
            });
        }
        else
        {
            saveBtn.addEventListener('click',function(e){
                title=document.getElementById('cmitem_title');
                desc=document.getElementById('cmitem_desc');
                price=document.getElementById('cmitem_price');
                image=document.getElementById('cmitem_image');
                MBMenuAddItemUpdateItem(object,{sta:true,title:title.value,desc:desc.value,price:price.value,image:image.value,allr:null,cal:null});
            });
        }

        title=document.getElementById('cmitem_title');
        title.focus();
        title.selectionStart = title.selectionEnd = title.value.length;
    }

    function MBMenuAddItemSaveItem(object,newobject,eventObject)
    {

        var date=new Date();
        var newId='m'+date.getYear()+date.getMonth()+date.getDate()+''+date.getHours()+''+date.getMinutes()+''+date.getSeconds();
        var newEl = {sta:newobject.sta,title:newobject.title,desc:newobject.desc,price:newobject.price,image:newobject.image,bcolor:'',props:'',allr:null,cal:null,include:false};

        if(MB.privateCurItemSides!=null)
        {
            var includes=[];
            for(var k in MB.privateCurItemSides){
                if(MB.privateCurItemSides.hasOwnProperty(k)){
                    includes.push(k);
                }
            }
            newEl['include']=includes.join(',');
        }

        var element;

        if(object.level==0){

            MB.menuObject[newId] = newEl;

            if(!MB.viewTree) {

                element = MBMenuItem(newEl, newId);
                MB.privateCurBlockLayer.appendChild(element);

            } else {

                element=MBMenuTreeItem(newEl,newId,0,true);
                separator=MBMenuTreeItemSeparator(newId,0);
                eventObject.parentNode.insertBefore(separator,eventObject);
                eventObject.parentNode.insertBefore(element,separator);

            }

            MBMenuCreateIndex();
            cRemoveTinyModal();

        } else {

            var curObject=MBMenuFindObject(object.id);

            if(!curObject.include){
                curObject.include={};
            }
            curObject.include[newId]=newEl;

            if(!MB.viewTree) {
                element = MBMenuItem(newEl, newId);
                MB.privateCurBlockLayer.appendChild(element);
                MB.privateCurItemSideLayer = null;
                MB.privateCurItemSides = null;
            }
            else
            {

                if(object.level==1){

                    element=MBMenuTreeItem(newEl,newId,1,true);
                    separator=MBMenuTreeItemSeparator(newId,1);
                    eventObject.parentNode.insertBefore(separator,eventObject);
                    eventObject.parentNode.insertBefore(element,separator);

                }
                else if (object.level==2){

                    element=MBMenuTreeItem(newEl,newId,2,true);
                    separator=MBMenuTreeItemSeparator(newId,2);
                    eventObject.parentNode.insertBefore(separator,eventObject);
                    eventObject.parentNode.insertBefore(element,separator);

                }

            }

            MBMenuCreateIndex();
            cRemoveTinyModal();
        }

    }

    function MBMenuAddItemUpdateItem(object,newobject)
    {

        var curObject=MBMenuFindObject(object.id);

        if(curObject){

            curObject['title']=newobject.title;
            curObject['desc']=newobject.desc;
            curObject['price']=newobject.price;
            curObject['image']=newobject.image;

            //console.log(MB.privateCurItemSides);

            if(MB.privateCurItemSides!=null){
                var includes=[];
                for(var k in MB.privateCurItemSides){
                    if(MB.privateCurItemSides.hasOwnProperty(k)){
                        includes.push(k);
                    }
                }
                curObject['include']=includes.join(',');
            }

            MBMenuCreateIndex();

            if(!MB.viewTree) {
                element = MBMenuItem(curObject, object.id);
                var oldElement = document.getElementById('mit' + object.id);
                oldElement.parentNode.insertBefore(element, oldElement);
                oldElement.parentNode.removeChild(oldElement);
            } else {
                var title=document.getElementById('mitl'+object.id);
                title.textContent=curObject['title'];
                var price=document.getElementById('mitp'+object.id);
                if(price!=null){
                    price.textContent=curObject['price'];
                }
            }

            cRemoveTinyModal();
            MB.privateCurItemSideLayer=null;
            MB.privateCurItemSides=null;

        }

    }

    function MBMenuDeleteItemQuery(object){

        var win=cOpenTinyModal({appTitle:"Edit Menu Item"});

        var control=document.createElement('div');
        var button=document.createElement('div');
        control.style.cssText='position:relative; height:85%; overflow-y:scroll; width:100%;';
        button.style.cssText='position:relative; height:15%; overflow:hidden; width:100%;';

        win.content.appendChild(control);
        win.content.appendChild(button);

        saveBtn=document.createElement('button');
        saveBtn.className='cine_fn_color_btn';
        saveBtn.style.width='256px';
        saveBtn.textContent='Yes Delete Item';
        saveBtn.addEventListener('click',function(e){MBMenuDeleteItem(object); cRemoveTinyModal();});
        cancelBtn=document.createElement('button');
        cancelBtn.className='cine_fn_color_btn';
        cancelBtn.style.width='256px';
        cancelBtn.textContent='No, I change my mind';
        cancelBtn.addEventListener('click',function(e){cRemoveTinyModal();});

        button.appendChild(saveBtn);
        button.appendChild(cancelBtn);

        control.innerHTML = '<p>You want to delete item from menu, are you sure that you want this action to be completed?</p> <p>If you trying to remove the root branch, all childs will also be removed.</p>';

    }

    function MBMenuDeleteItem(object)
    {

        var id=object.id;

        var curMeta=MB.menuIndex[id];
        var curObject;
        var curDeleted=false;

        if(curMeta.level==2)
        {
            curObject=MBMenuFindObject(curMeta.root);
            delete curObject.include[id];
            curDeleted=true;

        }
        else if(curMeta.level==3)
        {
            curObject=MBMenuFindObject(curMeta.subroot);
            delete curObject.include[id];
            curDeleted=true;

        }
        else {

            delete MB.menuObject[id];
            curDeleted=true;

        }

        if(curDeleted){

            var domElem=document.getElementById('mit'+id);
            var domElemSeparator=document.getElementById('s_'+id);

            if(domElem!=null){
                domElem.parentNode.removeChild(domElem);
            }
            if(domElemSeparator!=null){
                domElemSeparator.parentNode.removeChild(domElemSeparator);
            }

            MBMenuCreateIndex();

        }

    }

    // ---------------------------------------------------------------
    //
    //     Menu items inner window contents
    //
    // ---------------------------------------------------------------
    function MBMenuAddItemHTML(obj,data){

        if(typeof data==='undefined'){
            data={title:"",desc:"",price:0,image:"",include:{}};
        }

        MB.privateCurItemSides=null;

        var elems="";
        var main;
        var level=parseInt(obj.level);

        if(MB.viewTree){
            level--;
        }

        if(level < 2){

            main=document.createElement('div');
            main.className='cine_form_green';
            main.style.cssText='display:inline-block; vertical-align:top; padding:10px;';

            label=document.createElement('label');
            label.textContent='Group title';
            main.appendChild(label);

            title=document.createElement('input');
            title.setAttribute('id','cmitem_title');title.setAttribute('value',data.title);
            title.setAttribute('maxlength',32);
            title.className='cinedata';
            main.appendChild(title);

            label=document.createElement('label');
            label.textContent='Group description';
            main.appendChild(label);

            desc=document.createElement('textarea');
            desc.setAttribute('id','cmitem_desc');desc.setAttribute('style','height:64px');
            desc.className='cinedata';
            desc.textContent=data.desc;
            main.appendChild(desc);

            price=document.createElement('input');
            price.setAttribute('id','cmitem_price');price.setAttribute('type','hidden');
            price.setAttribute('value',data.price);price.setAttribute('maxlength',5);
            price.className='cinedata';
            main.appendChild(price);

            image=document.createElement('input');
            image.setAttribute('id','cmitem_image');image.setAttribute('type','hidden');
            image.setAttribute('value',data.image);image.setAttribute('maxlength',5);
            image.className='cinedata';
            main.appendChild(image);

        }
        else
        {

            main=document.createElement('div');
            main.className='cine_form_green';
            main.style.cssText='display:inline-block; vertical-align:top; padding:10px;';

            // Left Block -----------------------------------
            var mainLeft=document.createElement('div');
            mainLeft.className='cine_form_green';
            mainLeft.style.cssText='display:inline-block; vertical-align:top; padding:10px; width:50%';

            label=document.createElement('label');
            label.textContent='Group title';
            mainLeft.appendChild(label);

            title=document.createElement('input');
            title.setAttribute('id','cmitem_title');title.setAttribute('value',data.title);
            title.setAttribute('maxlength',32);
            title.className='cinedata';
            mainLeft.appendChild(title);

            label=document.createElement('label');
            label.textContent='Group description';
            mainLeft.appendChild(label);

            desc=document.createElement('textarea');
            desc.setAttribute('id','cmitem_desc');desc.setAttribute('style','height:64px');
            desc.className='cinedata';
            desc.textContent=data.desc;
            mainLeft.appendChild(desc);

            price=document.createElement('input');
            price.setAttribute('id','cmitem_price');
            price.setAttribute('value',data.price);price.setAttribute('maxlength',5);
            price.className='cinedata';
            mainLeft.appendChild(price);

            image=document.createElement('input');
            image.setAttribute('id','cmitem_image');image.setAttribute('type','hidden');
            image.setAttribute('value',data.image);image.setAttribute('maxlength',5);
            image.className='cinedata';
            mainLeft.appendChild(image);


            // Right Block -----------------------------------
            var mainRight=document.createElement('div');
            mainRight.className='cine_form_green';
            mainRight.style.cssText='display:inline-block; vertical-align:top; padding:10px; width:50%; text-align:left;';
            MB.privateCurItemSideLayer=mainRight;
            MB.privateCurItemSides={};

            var sideMenu=MB.sideObject;
            var checkedIn=data.include;

            if(checkedIn.length>2){
                checkedIn=checkedIn.split(',');
            } else {
                checkedIn=[];
            }

            if(sideMenu)
            {

                for(var k in sideMenu){
                    if(sideMenu.hasOwnProperty(k))
                    {
                        cur=sideMenu[k];
                        sideElm=document.createElement('div');
                        sideElm.setAttribute('id','cmiv_'+k);

                        if(arrayIndex(checkedIn,k)){
                            sideElm.className='sideElementBlockMiniActive';
                            MB.privateCurItemSides[k]=1;
                        } else {
                            sideElm.className='sideElementBlockMini';
                        }

                        sideElm.textContent=cur['name'];
                        sideElm.addEventListener('click',function(e){MBMenuItemVariantSwitch(e,this);}.bind(k));
                        mainRight.appendChild(sideElm);
                    }

                }

            }

            main.appendChild(mainLeft);
            main.appendChild(mainRight);

        }

        return main;
    }

    function MBMenuItemVariantSwitch(event,id)
    {
        var target=event.target;
        var parent=target.parentNode;

        if(target.className=='sideElementBlockMini'){
            target.className='sideElementBlockMiniActive';
            if(MB.privateCurItemSides!=null){
                if(!MB.privateCurItemSides.hasOwnProperty(id)){
                    MB.privateCurItemSides[id]=1;
                }
            }
        } else {
            target.className='sideElementBlockMini';
            if(MB.privateCurItemSides!=null){
                if(MB.privateCurItemSides.hasOwnProperty(id)){
                    delete MB.privateCurItemSides[id];
                }
            }
        }

    }

    function MBMenuTreeItem(object,id,level,newItem){

        newItem = (typeof newItem!=='undefined')?newItem:false;

        var layerClass=['','menu-first-level','menu-second-level','menu-third-level'];
        var priceClass=['','','','menu-third-price'];

        var layer=document.createElement('div');
        layer.className=layerClass[level+1];
        layer.setAttribute('id','mit'+id);
        layer.setAttribute('draggable','true');
        layer.setAttribute("ondragstart","MB.dragStart(this,'"+id+"');");
        layer.setAttribute("ondragend","MB.dragEnd(this);");

        header=document.createElement('div');
        header.className='menu-level-header';

        drag=document.createElement('button');
        drag.className='menu-button-drag';
        drag.innerHTML='<i class="fa fa-arrows"></i>';
        header.appendChild(drag);

        label=document.createElement('label');
        label.setAttribute('id','mitl'+id);
        label.setAttribute('onclick',"MB.editInline(this,'"+id+"','title');");
        label.innerHTML=object.title;
        header.appendChild(label);

        if(level==2)
        {
            price = document.createElement('div');
            price.setAttribute('id', 'mitp' + id);
            price.setAttribute('onclick',"MB.editInline(this,'"+id+"','price');");
            price.textContent = '$ '+ object.price;
            price.className = 'menu-third-price';
            header.appendChild(price);
        }

        del=document.createElement('button');
        del.className='menu-button-delete';
        del.setAttribute('onclick','MB.deleteItem(this,"'+id+'");');
        del.textContent='del';
        header.appendChild(del);

        edit=document.createElement('button');
        edit.className='menu-button-edit';
        edit.setAttribute('onclick','MB.editItem(this,"'+id+'");');
        edit.textContent='edit';
        header.appendChild(edit);

        if(level>0) {
            var clone = document.createElement('button');
            clone.className = 'menu-button-clone';
            clone.setAttribute('onclick', 'MB.cloneItem(this,"' + id + '");');
            clone.innerHTML = '<i class="fa fa-angle-double-down"></i>';
            header.appendChild(clone);
        }

        layer.appendChild(header);

        if(newItem){

            if(level<2)
            {
                var addBtnClass = ['menu-first-add', 'menu-second-add', 'menu-third-add'];
                var addBtnTexts = ['Add New Menu', 'Add New Category', 'Add New Item'];
                layeradd = document.createElement('div');
                layeradd.className = addBtnClass[level+1];
                layeradd.textContent = addBtnTexts[level+1];
                layeradd.setAttribute('onclick', 'MB.addTreeElement(this,"' + id + '")');
                layer.appendChild(layeradd);
            }

        }

        return layer;

    }

    function MBMenuTreeItemSeparator(id,lvl){

        var separator = document.createElement('div');
        separator.className = 'menu-drag-sept';
        separator.setAttribute('id','s_'+id);
        separator.setAttribute('parent',id);
        separator.setAttribute('level',lvl);
        separator.setAttribute('ondragover','MB.dragOver(event,this);');
        separator.setAttribute('ondragleave','MB.dragLeave(event,this);');
        separator.setAttribute('ondrop',"MB.dragDrop(event,this);");

        return separator;

    }

    function MBMenuEditInline(e,id,type){

        var offset = e.getBoundingClientRect();

        var fill = document.createElement('div');
        var form = document.createElement('form');
        var layer = document.createElement('div');
        var input = document.createElement('input');
        var submit = document.createElement('input');

        fill.style.cssText='position:absolute; width:100%; height:100%; background-color:rgba(200,200,200,0.2); z-index:1000; top:0px; left:0px;';
        layer.style.cssText='width:320px; height:32px; position:absolute; top:'+(offset.top-14)+'px; left:'+offset.left+'px; ';
        form.style.cssText='display:inline-block;';
        input.style.cssText='width:200px; height:46px; position:relative; display:inline-block;';

        submit.className='cine_fn_color_btn';
        submit.textContent='Apply';
        submit.setAttribute('type','submit');

        var clickedElement=e;
        var clickedType=type;

        form.addEventListener('submit',function(e){

            e.preventDefault();
            var value=input.value;

            var inputValue=MBMenuFindObject(id);
            if(inputValue) {
                if (clickedType == 'price') {
                    clickedElement.textContent = '$ ' + value;
                    inputValue.price=Number(value);
                } else {
                    clickedElement.textContent = value;
                    inputValue.title=value;
                }
            }

            fill.parentNode.removeChild(fill);

        });

        var inputValue=MBMenuFindObject(id);
        if(inputValue){
            if(type=='title'){
                input.setAttribute('value',inputValue.title);
            } else if (type=='price'){
                input.setAttribute('value',inputValue.price);
            }
        }

        layer.appendChild(form);
        form.appendChild(input);
        form.appendChild(submit);
        fill.appendChild(layer);

        document.body.appendChild(fill);
        input.focus();
        input.selectionStart = input.selectionEnd = input.value.length;
    }

    function MBMenuCloneItem(e,id){

        var root,include,element,newElement,key,
            itemMeta = MB.menuIndex[id],
            newIncludeObject={};
            var date=new Date();
            var newNumber=Number(date.getYear()+date.getMonth()+date.getDate()+''+date.getHours()+''+date.getMinutes()+''+date.getSeconds()+''+date.getMilliseconds());
            var newId='m'+newNumber;
            var i=1;

        var isAdded=false;

        if(itemMeta.subroot){

            root=MBMenuFindObject(itemMeta.subroot);
            include=root.include;
            element=include[id];
            newElement=clone(element);

            for(key in include){
                if(include.hasOwnProperty(key)){
                    newIncludeObject[key]=include[key];
                    if(key==id){
                        newIncludeObject[newId]=newElement;
                    }
                }
            }

            delete root['include'];
            root.include=newIncludeObject;
            isAdded=true;

        } else if (itemMeta.root) {

            root=MBMenuFindObject(itemMeta.root);
            include=root.include;
            element=include[id];
            newElement=clone(element);

            if(newElement.include){

                newInclude=newElement.include;
                newIncludeTemp={};

                for(var ic in newInclude)
                {
                    if(newInclude.hasOwnProperty(ic)){
                        newIncludeTemp['m'+(newNumber+i)]=newInclude[ic];
                    }
                    i++;
                }

                delete newElement['include'];
                newElement.include=newIncludeTemp;

            }

            for(key in include){
                if(include.hasOwnProperty(key)){
                    newIncludeObject[key]=include[key];
                    if(key==id){
                        newIncludeObject[newId]=newElement;
                    }
                }
            }


            delete root['include'];
            root.include=newIncludeObject;
            isAdded=true;

        }

        if(isAdded){

            MBMenuCreateIndex();
            var newItemMeta=MB.menuIndex[newId];
            console.log(newItemMeta);

            var itemLayer=document.getElementById('mit'+id);
            var itemSeparator=itemLayer.nextSibling;
            var insertIn=itemSeparator.nextSibling;

            element=MBMenuTreeItem(newElement,newId,itemMeta.level-1,true);
            var separator=MBMenuTreeItemSeparator(newId,itemMeta.level-1);

            if(newItemMeta.level==2 && newElement.include)
            {

                var newElLastChild=element.lastChild;
                itemContent=newElement.include;

                for(key in itemContent)
                {
                    if(itemContent.hasOwnProperty(key)) {
                        subElement = MBMenuTreeItem(itemContent[key], key, newItemMeta.level);
                        subSeparator = MBMenuTreeItemSeparator(newId, newItemMeta.level);
                        newElLastChild.parentNode.insertBefore(subSeparator,newElLastChild);
                        newElLastChild.parentNode.insertBefore(subElement,subSeparator);
                    }
                }

            }

            insertIn.parentNode.insertBefore(separator,insertIn);
            insertIn.parentNode.insertBefore(element,separator);

        }

    }

    function MBMenuDragOver(e,item){
        e.preventDefault();
        var lvl = parseInt(item.getAttribute('level'));
        if(lvl+1 == MB.dragObject[0].level)
            item.className = 'menu-drag-sept menu-drag-sept-over';
    }
    function MBMenuDragLeave(e,item){
        e.preventDefault();
        item.className='menu-drag-sept';
    }

    // ***********************************                                  ***************************************
    // ----------------------------------- DRAG'N'DROP DROP SWAP FUNCTIONAL ---------------------------------------
    // ***********************************                                  ***************************************

    function MBMenuDragDrop(e,item){

        e.preventDefault();
        e.stopPropagation();

        var obj=MB.dragObject[0];
        var id=item.getAttribute('parent');
        var lvl=parseInt(item.getAttribute('level'));

        var oldMenuItemRoot, oldMenuItemRootInclude, oldMenuItem, newMenuItemMeta, newMenuItemRoot, newIncludeObject,
            included, itemToDelete, itemSepToDelete, itemToDeleteContent, insertIn, k;
        if(id!=obj.id){

            if(lvl+1 == obj.level){

                if(obj.level==1){


                } else if (obj.level==2) {

                    // Swap Elements For The 2nd Level of Menu
                    oldMenuItemRoot = MBMenuFindObject(obj.root);
                    oldMenuItemRootInclude = oldMenuItemRoot.include;
                    oldMenuItem = clone(MBMenuFindObject(obj.id));
                    delete oldMenuItemRootInclude[obj.id];

                    newMenuItemMeta = MB.menuIndex[id];
                    newMenuItemRoot = MBMenuFindObject(newMenuItemMeta.root);

                    newIncludeObject={};

                    included=newMenuItemRoot.include;

                    for(k in included){
                        if(included.hasOwnProperty(k)){

                            newIncludeObject[k]=included[k];
                            if(k==id){
                                newIncludeObject[obj.id]=oldMenuItem;
                            }

                        }
                    }

                    delete newMenuItemRoot['include'];
                    newMenuItemRoot['include']=newIncludeObject;

                    itemToDelete=document.getElementById('mit'+obj.id);
                    itemToDeleteContent=itemToDelete.childNodes;
                    itemToDeleteCLength=itemToDeleteContent.length;
                    itemSepToDelete=itemToDelete.nextSibling;

                    insertIn=item.nextSibling;
                    element=MBMenuTreeItem(oldMenuItem,obj.id,1);

                    for(var i=0;i<itemToDeleteCLength;i++){
                        cur=itemToDeleteContent[i];
                        node=cur.cloneNode(true);
                        if(typeof cur!=='undefined' && node.className!='menu-level-header')
                            element.appendChild(node);
                    }

                    separator=MBMenuTreeItemSeparator(obj.id,1);

                    itemToDelete.parentNode.removeChild(itemToDelete);
                    itemSepToDelete.parentNode.removeChild(itemSepToDelete);

                    insertIn.parentNode.insertBefore(separator,insertIn);
                    insertIn.parentNode.insertBefore(element,separator);

                } else if (obj.level==3) {

                    // Swap Elements For The 3rd Level of Menu
                    oldMenuItemRoot = MBMenuFindObject(obj.subroot);
                    oldMenuItemRootInclude = oldMenuItemRoot.include;
                    oldMenuItem = clone(MBMenuFindObject(obj.id));
                    delete oldMenuItemRootInclude[obj.id];

                    newMenuItemMeta = MB.menuIndex[id];
                    newMenuItemRoot = MBMenuFindObject(newMenuItemMeta.subroot);

                    newIncludeObject={};

                    included=newMenuItemRoot.include;

                    for(k in included){
                        if(included.hasOwnProperty(k)){

                            newIncludeObject[k]=included[k];
                            //console.log(included[k].title);
                            if(k==id){
                                newIncludeObject[obj.id]=oldMenuItem;
                                //console.log(oldMenuItem.title);
                            }

                        }
                    }

                    delete newMenuItemRoot['include'];
                    newMenuItemRoot['include']=newIncludeObject;

                    itemToDelete=document.getElementById('mit'+obj.id);
                    itemSepToDelete=itemToDelete.nextSibling;
                    itemToDelete.parentNode.removeChild(itemToDelete);
                    itemSepToDelete.parentNode.removeChild(itemSepToDelete);

                    insertIn=item.nextSibling;
                    element=MBMenuTreeItem(oldMenuItem,obj.id,2);
                    separator=MBMenuTreeItemSeparator(obj.id,2);
                    insertIn.parentNode.insertBefore(separator,insertIn);
                    insertIn.parentNode.insertBefore(element,separator);


                }

                MBMenuCreateIndex();
            }

            item.className='menu-drag-sept';

        } else {

            item.className='menu-drag-sept';

        }
    }

    function MBMenuItem(object,id){

        var menuitem,txt,title,desc,img;

        title=object['title'];
        desc=object['desc'];
        img=object['image'];

        menuitem=document.createElement('div');
        menuitem.className = 'cine_fn_resto_itembox';

        txt=document.createElement('h4');
        txt.textContent=title;
        menuitem.appendChild(txt);
        txt=document.createElement('p');
        txt.textContent=desc;
        menuitem.appendChild(txt);

        menuitem.setAttribute('id','mit'+id);
        menuitem.addEventListener('click',function(e){MBMenuDrawLevel(this)}.bind(id));

        return menuitem;
    }

    function MBMenuFindObject(id){

        var index=MB.menuIndex;

        if(index.hasOwnProperty(id)){

            var me=index[id];

            if(me.level==1){
                return MB.menuObject[id];
            }
            else if(me.level==2){
                rindex=me.root;
                return MB.menuObject[rindex]['include'][id];
            }
            else if(me.level==3){
                rindex=me.root;
                sindex=me.subroot;
                return MB.menuObject[rindex]['include'][sindex]['include'][id];
            }

        }

        return false;
    }

    function MBMenuCreateIndex(){

        if(MB.menuObject!=null)
        {

            MB.menuIndex={};
            MB.menuIndexLength=0;
            MB.menuIndexHeader=[];

            var menu=MB.menuObject;
            var k,c,sk,sc,tk,tc,ok,sok,tok;

            for (k in menu)
            {

                ok = false;
                if(menu.hasOwnProperty(k)) {

                    // 1st level
                    c = menu[k];
                    if (c['include']) {
                        try {
                            ok = Object.keys(c['include']);
                        } catch (e){
                            ok = false;
                        }
                    }

                    MB.menuIndexHeader.push(k);
                    MB.menuIndex[k] = {id:k, root: false, subroot: false, level: 1, title: c['title'], include:ok};
                    MB.menuIndexLength++;

                    if(ok){

                        // 2nd level
                        sok=false;
                        for(sk in c.include){

                            if(c.include.hasOwnProperty(sk)) {
                                sc = c.include[sk];
                                if (sc['include']) {
                                    try {
                                        sok = Object.keys(sc['include']);
                                    } catch (e){
                                        sok = false;
                                    }
                                }
                                MB.menuIndexHeader.push(sk);
                                MB.menuIndex[sk] = {id:sk, root:k, subroot: false, level: 2, title: sc['title'], include:sok};
                                MB.menuIndexLength++;

                                if(sok){

                                    tok=false;
                                    for(tk in sc.include){

                                        if(sc.include.hasOwnProperty(tk)) {
                                            tc = sc.include[tk];
                                            if (tc['include']) {
                                                try {
                                                    tok = Object.keys(tc['include']);
                                                } catch (e){
                                                    tok = false;
                                                }
                                            }
                                            MB.menuIndexHeader.push(tk);
                                            MB.menuIndex[tk] = {id:tk, root:k, subroot: sk, level: 3, title: tc['title'], include:tok};
                                            MB.menuIndexLength++;
                                        }

                                    }

                                }

                            }

                        }

                    }

                }

            }

        }

    }

    function MBSaveMenu(){

        XWF.connQueryObject = {query:"admindata", type:"putrestomenu"};

        XWF.connQueryData = {ident:MB.restaurant,user:0,data:JSON.stringify(MB.menuObject),adata:JSON.stringify(MB.sideObject)};

        MBUserKey(XWF.connQueryData);

        XWF.send({
            waitCallback:true,
            onComplete:MBMenuSaveFinish
        });

    }

    function MBMenuSaveFinish(data){

    }

    // ***************************************
    // ---------------------------------------
    // SIDES MENU VARIANTS AND SIDES
    // ---------------------------------------
    // ***************************************

    function MBOpenMenuSideBuilder(e)
    {
        win=XWF.openwindow({
            navigationBar:true,
            raiseWindow:true,
            changeTitle:'Menu Sides Editor',
            windowSubTitle:"Restaurant Side Variants Editor"
        });

        sides=MBOpenSideMenuBuilderDisplay();

        win.append(sides);
    }

    function MBOpenSideMenuBuilderDisplay(){

        var layer=document.createElement('div');
        layer.className='cine_fn_resto_cpanel';
        layer.style.cssText='padding-top:0;';
        layer.setAttribute('id','meditor_ls');

        var control=document.createElement('div');
        cntrl=MBMenuSideControlPanel();
        control.appendChild(cntrl);

        var blocks=document.createElement('div');
        blocks.className='cine_fn_resto_cpanel';
        blocks.style.cssText='padding-top:0;';

        layer.appendChild(control);
        layer.appendChild(blocks);

        MB.privateCurSideLayer=blocks;

        var sideMenu=MB.sideObject;

        if(sideMenu)
        {

            for(var k in sideMenu)
            {

                if(sideMenu.hasOwnProperty(k))
                {
                    cur=sideMenu[k];
                    sideElm=MBMenuSideRenderElem(cur,k);
                    blocks.appendChild(sideElm);

                }

            }

        }

        return layer;

    }

    // -----------------------------------------
    //
    //      Render Element For Side Menu
    //
    // -----------------------------------------
    function MBMenuSideRenderElem(obj,id){

        var sideElm=document.createElement('div');
        sideElm.className='sideElementBlock';
        sideElm.setAttribute('id','csme'+id);

        innerBlock=document.createElement('div');
        innerBlock.className='sideElementBlockInner';
        title=document.createElement('h4');
        title.textContent=obj['name'];

        innerBlock.appendChild(title);
        innerBlock.addEventListener('click',function(e){MBMenuSideEditItem(this)}.bind(id));

        sideElm.appendChild(innerBlock);

        deleteBtn=document.createElement('div');
        deleteBtn.className='sideElementBlockDelete';
        deleteBtn.textContent='Delete';
        deleteBtn.addEventListener('click',function(e){MBMenuSideElementDelete(this)}.bind(id));
        sideElm.appendChild(deleteBtn);

        return sideElm;

    }

    function MBMenuSideAddItem(e)
    {
        var win = cOpenTinyModal({appTitle:"Edit Side Item"});
        var control=document.createElement('div');
        var button=document.createElement('div');

        control.style.cssText='position:relative; height:85%; overflow-y:scroll; width:100%;';
        button.style.cssText='position:relative; height:15%; overflow:hidden; width:100%;';

        win.content.appendChild(control);
        win.content.appendChild(button);

        // Save Side Variant Button
        saveBtn=document.createElement('button');
        saveBtn.className='cine_fn_color_btn';
        saveBtn.textContent='Save';
        saveBtn.addEventListener('click',function(e){MBMenuSideVariantSave(true);});

        // Cancel of Save/Edit Form Button
        cancelBtn=document.createElement('button');
        cancelBtn.className='cine_fn_color_btn';
        cancelBtn.textContent='Cancel';
        cancelBtn.addEventListener('click',function(e){cRemoveTinyModal(); MB.privateCurSideVarLayer = null;});

        button.appendChild(saveBtn);
        button.appendChild(cancelBtn);

        var cur={name:"",price:"",desc:"",image:"",variant:{}};
        var id="";

        var leftC=MBMenuSideEditLeftColumn(cur,id);
        var rightC=MBMenuSideEditRightColumn(cur,id);

        control.appendChild(leftC);
        control.appendChild(rightC);

    }

    function MBMenuSideEditItem(id)
    {
        var win = cOpenTinyModal({appTitle:"Edit Side Item"});
        var control=document.createElement('div');
        var button=document.createElement('div');

        control.style.cssText='position:relative; height:85%; overflow-y:scroll; width:100%;';
        button.style.cssText='position:relative; height:15%; overflow:hidden; width:100%;';

        win.content.appendChild(control);
        win.content.appendChild(button);

        // Save Side Variant Button
        saveBtn=document.createElement('button');
        saveBtn.className='cine_fn_color_btn';
        saveBtn.textContent='Save';
        saveBtn.addEventListener('click',function(e){MBMenuSideVariantSave(false);});

        // Cancel of Save/Edit Form Button
        cancelBtn=document.createElement('button');
        cancelBtn.className='cine_fn_color_btn';
        cancelBtn.textContent='Cancel';
        cancelBtn.addEventListener('click',function(e){cRemoveTinyModal(); MB.privateCurSideVarLayer = null;});

        button.appendChild(saveBtn);
        button.appendChild(cancelBtn);

        if(MB.sideObject.hasOwnProperty(id)){

            var cur=MB.sideObject[id];

            var leftC=MBMenuSideEditLeftColumn(cur,id,false);
            var rightC=MBMenuSideEditRightColumn(cur,id);

            control.appendChild(leftC);
            control.appendChild(rightC);

        }
    }

    function MBMenuSideEditLeftColumn(obj,id,type){

        if(typeof type==='undefined'){
            type=true;
        }

        var mainParam=document.createElement('div');
        mainParam.className='cine_form_green';
        mainParam.style.cssText='display:inline-block; vertical-align:top; width:50%; padding:10px;';

        var ident = document.createElement('input');
        if(type) {
            var labelident = document.createElement('label');
            labelident.textContent = 'Identificator';
            mainParam.appendChild(labelident);
        } else {
            ident.setAttribute('type', 'hidden');
        }
        ident.setAttribute('id', 'cmsitem_ident');
        ident.setAttribute('value', id);
        mainParam.appendChild(ident);


        var labeltitle=document.createElement('label');
        labeltitle.textContent='Title';
        var title=document.createElement('input');
        title.setAttribute('id','cmsitem_title');
        title.setAttribute('value',obj.name);

        var labelprice=document.createElement('label');
        labelprice.textContent='Price';
        var price=document.createElement('input');
        price.setAttribute('id','cmsitem_price');
        price.setAttribute('value',obj.price);

        var image=document.createElement('input');
        image.setAttribute('id','cmsitem_image');
        image.setAttribute('type','hidden');
        image.setAttribute('value',obj.image);

        var labeldesc=document.createElement('label');
        labeldesc.textContent='Description';
        var descript=document.createElement('textarea');
        descript.setAttribute('id','cmsitem_desc');
        descript.textContent=obj.desc;


        mainParam.appendChild(labeltitle);
        mainParam.appendChild(title);
        mainParam.appendChild(labelprice);
        mainParam.appendChild(price);
        mainParam.appendChild(image);
        mainParam.appendChild(labeldesc);
        mainParam.appendChild(descript);

        return mainParam;

    }

    function MBMenuSideEditRightColumn(obj,id){

        var varParam=document.createElement('div');
        varParam.style.cssText='display:inline-block; vertical-align:top; width:50%; padding:10px; padding-top: 32px;';

        MB.privateCurSideVarLayer = varParam;

        var sideVariants=obj.variant;
        for (var k in sideVariants){

            if(sideVariants.hasOwnProperty(k)){

                cvar=MBMenuSideAddVariantToSide({name:k,value:Number(sideVariants[k])});
                varParam.appendChild(cvar);

            }

        }



        var addButton=document.createElement('button');
        addButton.className='cine_fn_green_btn';
        addButton.textContent='Add New Variant';
        addButton.addEventListener('click',function(e){

            var variant=MBMenuSideAddVariantToSide();
            var lastElem=this.lastChild;
            this.insertBefore(variant,lastElem);

        }.bind(varParam));

        varParam.appendChild(addButton);

        return varParam;
    }

    function MBMenuSideAddVariantToSide(data){

        if(typeof data==='undefined'){
            data={name:"",value:-1};
        }

        var variant=document.createElement('div');
        variant.className='c_side_variant';
        var name=document.createElement('input');
        name.className='c_side_variant_name';
        name.setAttribute('value',data.name);

        pvalue=data.value;
        textval="";
        if(pvalue==-1){
            textval='Empty';
        } else if(pvalue==0){
            textval='Free';
        } else {
            textval=pvalue;
        }
        var price=document.createElement('input');
        price.className='c_side_variant_price';
        price.setAttribute('value',textval);

        price.addEventListener('click',function(e){MBMenuSideVariantPricePopup(this)}.bind(price));

        variant.appendChild(name);
        variant.appendChild(price);

        return variant;

    }

    function MBMenuSideVariantPricePopup(initiator){

        var initL=initiator.offsetLeft;
        var initT=initiator.offsetTop;

        var popup = document.createElement('div');
        popup.style.cssText="position:absolute; top:"+initT+"px; left:"+(initL-50)+"px; background-color:#ff5c61; width:200px; height:auto; border-radius:6px;";

        var empty=document.createElement('button');
        empty.className='cine_fn_green_btn';
        empty.textContent='Empty value';
        empty.addEventListener('click',function(e){initiator.value='Empty'; popup.parentNode.removeChild(popup);});

        var free=document.createElement('button');
        free.className='cine_fn_green_btn';
        free.textContent='Free';
        free.addEventListener('click',function(e){initiator.value='Free'; popup.parentNode.removeChild(popup);});

        var number=document.createElement('button');
        number.className='cine_fn_green_btn';
        number.textContent='Manual';
        number.addEventListener('click',function(e){MBMenuSideVariantPricePopupSetPrice(this.popup,this.init)}.bind({popup:popup,init:initiator}));

        popup.appendChild(empty);
        popup.appendChild(free);
        popup.appendChild(number);

        initiator.parentNode.parentNode.appendChild(popup);

    }

    function MBMenuSideVariantPricePopupSetPrice(popup,init){

        popup.innerHTML="";

        var value=document.createElement('input');
        value.style.cssText = "width:96%; border-radius:6px; border:1px solid #CCCCCC; background:#FFFFFF; font-size:24px; margin:2%";
        value.setAttribute('value','0');

        var button=document.createElement('button');
        button.className='cine_fn_green_btn';
        button.textContent='Set value';
        button.addEventListener('click',function(e){

            var val=Number(this.value);

            if(val>0){
                init.value=this.value
            } else if (val==0){
                init.value='Free';
            } else {
                init.value='Empty';
            }

            popup.parentNode.removeChild(popup);

        }.bind(value));

        popup.appendChild(value);
        popup.appendChild(button);

    }

    function MBMenuSideControlPanel(){

        var o=document.createElement('div');
        o.className='cine_fn_cbar';

        //Add
        var a = document.createElement('a');
        a.className = 'cine_fn_cbar_elem';
        a.textContent = ' Add Side Item ';
        a.addEventListener('click', function (e) {
            MBMenuSideAddItem(e);
        });
        o.appendChild(a);

        //Delete
        a = document.createElement('a');
        a.className = 'cine_fn_cbar_elem';
        a.textContent = ' Delete Items ';
        a.addEventListener('click', function (e) {
            MBMenuSideElementDelActivate();
        });
        o.appendChild(a);



        //Save
        a=document.createElement('a');
        a.className='cine_fn_cbar_elem_r';
        a.textContent=' Save Side Menu ';
        a.addEventListener('click',function(e){
            MBSaveSideMenu(e);}
        );
        o.appendChild(a);

        return o;

    }

    function MBMenuSideVariantSave(type){

        var ident=document.getElementById('cmsitem_ident');
        var title=document.getElementById('cmsitem_title');
        var price=document.getElementById('cmsitem_price');
        var image=document.getElementById('cmsitem_image');
        var desc=document.getElementById('cmsitem_desc');

        if(MB.privateCurSideVarLayer!=null){

            var variants={};
            var flds=MB.privateCurSideVarLayer.childNodes;
            var len=flds.length;

            for(var i=0;i<len;i++){

                cur=flds[i];
                if(cur.className=='c_side_variant'){

                    params=cur.childNodes;
                    vname=params[0].value;
                    vprice=params[1].value;

                    if(vprice=='Empty'){
                        vprice=-1;
                    } else if (vprice=='Free'){
                        vprice=0;
                    } else {
                        vprice=Number(vprice);
                    }

                    if(vname.length>0)
                        variants[vname]=vprice;

                }

            }

            var object={name:title.value,price:price.value,desc:desc.value,image:image.value,variant:variants};
            var id=ident.value.toLowerCase();

            if(type) {

                if(!MB.sideObject.hasOwnProperty(id))
                {
                    MB.sideObject[id]=object;

                    sideElement=MBMenuSideRenderElem(object,id);
                    MB.privateCurSideLayer.appendChild(sideElement);

                    cRemoveTinyModal();
                }
                else {

                    i=0;
                    var limit=0;
                    do {
                        i++;
                        limit++;
                        if(limit>100){
                            break;
                        }
                    } while (MB.sideObject.hasOwnProperty(id+""+i));

                    MB.sideObject[id+""+i]=object;

                    sideElement=MBMenuSideRenderElem(object,id+""+i);
                    MB.privateCurSideLayer.appendChild(sideElement);
                    cRemoveTinyModal();
                }

            } else {

                if(MB.sideObject.hasOwnProperty(id))
                {
                    MB.sideObject[id]=object;

                    var visibleElement=document.getElementById('csme'+id);

                    if(visibleElement!=null)
                    {

                        sideElement=MBMenuSideRenderElem(object,id);

                        MB.privateCurSideLayer.insertBefore(sideElement,visibleElement);
                        MB.privateCurSideLayer.removeChild(visibleElement);

                    }
                    else
                    {
                        alert('Information cannot be updated, please reload this form');
                    }

                    cRemoveTinyModal();

                }
                else
                {
                    alert('Element was not found and cannot be edited');
                }

            }


        }

        return false;

    }

    function MBMenuSideElementDelActivate(){

        var objects=document.getElementsByClassName('sideElementBlock');
        var length=objects.length;
        var type=false;
        var vset="";

        var cur;

        for(var i=0;i<length;i++){

            cur=objects[i];
            lst=cur.lastChild;
            display=lst.style.display;

            if(!type){
                if(display=='none'){
                    vset='block';
                } else {
                    vset='none';
                }
                lst.style.display=vset;
            } else {
                lst.style.display=vset;
            }

        }

    }

    function MBMenuSideElementDelete(id){

        if(MB.sideObject.hasOwnProperty(id))
        {
            delete MB.sideObject[id];
            var visibleElement=document.getElementById('csme'+id);

            if(visibleElement!=null)
            {
                visibleElement.parentNode.removeChild(visibleElement);
            }
        }

    }

    function MBSaveSideMenu(e){

        XWF.connQueryObject = {query:"admindata", type:"putsidemenu"};
        XWF.connQueryData = {ident:MB.restaurant,user:0,data:JSON.stringify(MB.sideObject)};

        MBUserKey(XWF.connQueryData);

        XWF.send({
            waitCallback:true,
            onComplete:MBMenuSideSaveFinish
        });

    }

    function MBMenuSideSaveFinish(data){



    }

    // ***************************************
    // ---------------------------------------
    // SPECIAL FUNCTIONS
    // ---------------------------------------
    // ***************************************

    function arrayIndex(a,e){
        var l=a.length;for(var i=0;i<l;i++){if(a[i]===e)return {result:true,pos:i}}return false;
    }

    function arrayRemove(arr, index) {
        var n = arr.slice((index) + 1 || arr.length);
        arr.length = index < 0 ? arr.length + index : index;
        return arr.push.apply(arr,n);
    }

    function MBUserKey(object){

        if(window.localStorage!=='undefined'){
            object['user']=window.localStorage.getItem('user_id');
            object['userkey']=window.localStorage.getItem('user_key');
        } else {
            object['user']=0;
            object['userkey']="";
        }

    }

    if(!window.MB){window.MB = $cine();}

})();
