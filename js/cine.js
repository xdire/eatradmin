/**
 * Created by xdire on 29.09.14.
 */

var fnCurrentRestaurant=null;

var cineViewW=0;
var cineViewH=0;

var cineFnNotify=null;
var cineFnCurrentModal=null;

var cineFnWindows=[];
var cineFnLastWindow;
var cineFnCurrentWindow;

var cineFnMenuArray;
var cineFnSidesArray;

var cinePriceObject;
var cinePriceTitle;
var cineSideOptions;

var cOrderPrice={priceCash:0,taxCash:0,tipCash:0,totalCash:0,tipPercent:0,taxPercent:0};
var astype={number:'num',string:'str'};
var cineRestoTypes=[
    'American (Traditional)', 'American (New)', 'Italian', 'Latin American', 'Thai', 'Pizza',
    'Burgers', 'Mexican', 'Chinese', 'Asian Fusion', 'Greek', 'Mediterranean', 'Sushi',
    'Seafood', 'Steakhouse', 'Middle Eastern', 'Bistro', 'Gastropubs', 'French',
    'Breakfast & Brunch', 'Asian', 'Tapas/Small Plates', 'Bars', 'Irish', 'Pubs',
    'European', 'Indian', 'Japanese', 'Turkish', 'Cafes/Sandwiches'];

(function()
{
    var $cine = function(data){return new cineBox(data);};
    var $cineFnDeviceType=false;
    var $cineFnMainObjects={
        //queryBase:'http://eatnow.xdire.ru/',queryApi:'srv',queryStat:'stat',queryExt:'ext',
        //queryBase:'http://eatnow1212/',queryApi:'srv',queryStat:'stat',queryExt:'ext',
        queryBase:'https://api.myvoila.me/',queryApi:'srv',queryStat:'stat',queryExt:'ext',
        queryResto:'restoinfo',queryOrder:'restoorder',
        methodNavi:'get_location',methodUser:'userinfo',methodCard:'cardinfo',methodForm:'cardform',methodList:'getrestolist',methodCartId:'getorderident',
        pageAnims:'cine_fn_animations',
        pageSearch:'cine_fn_form_search',pageSearchInp:'cine_fn_loc_search',
        modalDefW:320,modalDefH:64,modalClass:'cine_fn_modal_default',modalHeader:'h3',
        domLayer:'div',domButton:'a',domButtonForm:'input',
        domForm:'form',domInput:'input', domSelect:'select',
        formCenter:'cine_fn_form_centerer',
        btnClass:'cine_fn_simple_btn',mbtnClass:'cine_fn_color_btn',
        reqPrefix:'cinereq_',infoPrefix:'cineinfo_',
        bTreeLink:'https://js.braintreegateway.com/v2/braintree.js'};

    var cineBox = function(ciobject){

        this.version='0.8';
        var mainIterObj=null;
        var mainIterCout=0;

// LOADING SECTION -----------------------------------------------------
        function cineBoxFnAppLoad(state,init){

            cineViewH=window.innerHeight;
            cineViewW=window.innerWidth;

            if(state){
                if(init !== null){
                    var cine = new cineBox();
                    for(i in init){

                        var c=init[i];

                        if(c=='page')
                            cine.init.page();
                        if(c=='user')
                            cine.init.userui(cine);

                    }
                }
            }
        }

        this.load = {
            sysChckTime:100,
            sysInitWith:null,
            sysChck: function(){
                mainIterCout++;
                if(mainIterCout>1000)
                    clearInterval(mainIterObj);
                if(document.readyState==="complete"){
                    clearInterval(mainIterObj);
                    cineBoxFnAppLoad(true,this.sysInitWith);
                }
            },
            sysInit: function(initwith){
                if(typeof initwith === 'string')
                    this.sysInitWith=initwith.split(',');
                mainIterObj=setInterval(this.sysChck.bind(this),this.sysChckTime);
            }
        };

// INITIALIZATION SECTION -----------------------------------------------------
        this.init = {

            page: function(){
                $cineFnDeviceType=true;
                cineIndexAfterLoad();
                //cineBoxFnAnimResize();

            },
            userui: function(cobj){

                var obj,r;
                var uib = document.getElementById('cine_fn_view_buttons');
                if(uib!==null) {
                    uib = uib.value;
                    uib = uib.split(',');
                    for (var u in uib) {
                        if (obj = document.getElementById(uib[u])) {
                            r = obj.getAttribute('cinereqtype');

                            if (r == 'view') {// obj.addEventListener('click',function(e){cobj.rexec.get(e);});
                            }
                            if (r == 'popup') { // obj.addEventListener('click',function(e){cobj.rexec.route(e);});
                            }
                            if (r == 'man') {// obj.addEventListener('click',function(e){cobj.rexec.getManual(e);});
                            }

                        }
                    }
                }

                if(typeof cineFnUserInit === 'function'){
                    cineFnUserInit();
                }

            }

        };

// AJAX SECTION -----------------------------------------------------
        this.ajaxfunc = {

            initObj: function(){
                var xhr = false;
                if (window.XMLHttpRequest){
                    xhr = new XMLHttpRequest();
                }return xhr;
            },
            postData: function(xhr,url,data,callback,backobj){
                xhr.open('POST',url,true);
                xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                xhr.onreadystatechange = function(){
                    if (xhr.readyState == 4 && xhr.status == 200){
                        callback(xhr.responseText);
                    }
                };
                data = cineBoxFnObjToPost(data,'ajax');
                xhr.send(data);
            }
        };

    this.userkey = function(object){

        if(window.localStorage!=='undefined'){
            object['user']=window.localStorage.getItem('user_id');
            object['userkey']=window.localStorage.getItem('user_key');
        } else {
            object['user']=0;
            object['userkey']="";
        }

    };

    this.userkeyupd = function(object){

        if(window.localStorage!=='undefined'){
            object['user']=window.localStorage.getItem('user_id');
            object['userkey']=window.localStorage.getItem('user_key');
        } else {
            object['user']=0;
            object['userkey']="";
        }
        return object;
    };

    this.error = function(o,e,m,r){
        cineBoxFnErrorParse(o,e,m,r);
    };

    this.action = {

        cardprocess: function(e,defer,deferfunc){
            cineBoxFnloadScript($cineFnMainObjects.bTreeLink,e,cineBoxFnProcessUserCard,defer,deferfunc);
        }

    };

    this.helper = {

        domTraverse: function(obj,param){
            return cineBoxFnDomTraverse(obj,param);
        },
        domToJson: function(domArray){
            return cineBoxFnEntityToJSON(domArray);
        },
        domCreateEl: function(t,i,c,d,cd,s,sd){
            return cineCreateDomEl(t,i,c,d,cd,s,sd);
        },
        findClass: function(s,c){
            return cineBoxFnReturnClass(s,c);
        },
        arrayClean: function(a){
            return cineBoxFnArrayFilled(a);
        },
        frameLabeled: function(o){
            return cineBoxLabelFrame(o);
        },
        timeToSelect: function(t){
            return cineMakeDays(t);
        }

    };

    this.resto = {
        getMenu: function(e,d){
            cineBoxFnRestoOrderMenu(e,d);
        },
        renderMenu: function(d,l,g,sg,i){
            cineBoxFnRestoRendrMenu(d,l,g,sg,i);
        }
    };

    this.menu = {
        sideSet: function(e){
            cineBoxFnRestoMenuSideSet(e);
        }
    };

    this.order = {
        coItems: function(o,i,k,f,d,s){
            return cineBoxFnRestoOrderItemList(o,i,k,f,d,s);
        },
        coSummary: function(p){
            return cineBoxFnRestoOrderItemSumm(p);
        },
        price: function(type){
            return this.retNumber(cOrderPrice.priceCash,type);
        },
        tax: function(type){
            return this.retNumber(cOrderPrice.taxCash,type);
        },
        tip: function(type){
            return this.retNumber(cOrderPrice.tipCash,type);
        },
        total: function(type){
            return this.retNumber(cOrderPrice.totalCash,type);
        },
        countPrices: function(p,t,u){
            cOrderPriceCount(p,t,u);
        },
        setNumbers: function(price,tax,tips,total,taxP,tipP){
            cOrderPrice.priceCash=price;
            cOrderPrice.taxCash=tax;
            cOrderPrice.tipCash=tips;
            cOrderPrice.totalCash=total;
            cOrderPrice.tipPercent=tipP;
            cOrderPrice.taxPercent=taxP;
        },
        retNumber: function(num,type){
            if(typeof type!=='undefined'){
                if(type=='num'){
                    return num;
                } else if (type=='str'){
                    return num.toFixed(2);
                }
            } else {
                return num;
            }
        }
    };

    this.toggler = function(funcOn,funcOff,initalState)
    {
        var className="";
        if(initalState){
            className='toggler togglerActive';
        } else {
            className='toggler togglerPassive';
        }

        var toggler=document.createElement('button');
        toggler.className=className;
        toggler.addEventListener('click',function(e){cineToggler(e,this.element,this.on,this.off)}.bind({element:toggler,on:funcOn,off:funcOff}));

        return toggler;
    }
};
    cineBox.prototype.ajax = function(type,url,data,event){
        if(type=='post'){
            var ajo = this.ajaxfunc.initObj();
            if(ajo){
                return this.ajaxfunc.postData(ajo,url,data,event);
            }
        }
    };

    function cineToggler(ev,toggler,funcOn,funcOff){

        if(toggler.className=='toggler togglerActive'){

            if(typeof funcOff==='function'){
                funcOff(ev,function(){cineTogglerToggle(toggler,false)});
            }

        } else {

            if(typeof funcOn==='function'){
                funcOn(ev,function(){cineTogglerToggle(toggler,true)});
            }

        }

    }

    function cineTogglerToggle(element,status){

        if(status){
            element.className='toggler togglerActive';
        } else {
            element.className='toggler togglerPassive';
        }

    }

    // AJAX HELPER SERIALIZE TO URL // JS Object notation {"key":value,...} to key=value&....
    function cineBoxFnObjToPost(data,qtype){
        var r = [];
        for (var k in data){
            r.push(encodeURIComponent(k) + '=' + encodeURIComponent(data[k]));
        }r.push('query_type='+qtype); return r.join('&');
    }

    function cineBoxFnErrorParse(data,error,errorMsg,repeatFunc){

        if(error==1000)
        {
            var loginObject;
            var windowContent=data.content;

            if(windowContent!=null)
            {

                loginObject=cineBoxFnRequireLogin(windowContent,repeatFunc);
                windowContent.appendChild(loginObject);


            } else {

                obj=document.getElementsByClassName('cineRoot');
                loginObject=cineBoxFnRequireLogin(obj[0],repeatFunc);
                obj[0].style.textAlign='center';
                obj[0].appendChild(loginObject);

            }
            return false;
        }

    }

    function cineBoxFnRequireSignup(e,o,r,form){

        var uemail='cine_fn_email';
        var uname='cine_fn_username';
        var upass='cine_fn_password';
        var upassr='cine_fn_password2';
        var ufname='cine_fn_firstname';
        var ulname='cine_fn_lastname';

        var f=document.createElement('form');
        f.className='cine_fn_form_default';
        var h='';

        h+='<label>Email address</label>';
        h+='<input type="text" name="'+uemail+'" id="'+uemail+'" value="">';
        h+='<label>Unique Login</label>';
        h+='<input type="text" name="'+uname+'" id="'+uname+'" value="">';
        h+='<label>Password</label>';
        h+='<input type="password" name="'+upass+'" id="'+upass+'" value="">';
        h+='<label>Repeat Password</label>';
        h+='<input type="password" name="'+upassr+'" id="'+upassr+'" value="">';
        h+='<label>First Name</label>';
        h+='<input type="text" name="'+ufname+'" id="'+ufname+'" value="">';
        h+='<label>Lastname</label>';
        h+='<input type="text" name="'+ulname+'" id="'+ulname+'" value="">';

        var id=form;

        f.innerHTML=h;
        f.style.marginTop='40px';
        var snb=document.createElement('button');
        snb.className='cine_fn_color_btn';
        snb.style.width=200+'px';
        snb.setAttribute('type','button');
        snb.innerHTML='Sign up';
        snb.setAttribute('id','cine_login_button');
        snb.addEventListener('click',function(e){cineBoxFnProcessRegister(e,this.func,this.creds,this.form)}
            .bind({func:r,creds:{email:uemail,name:uname,pass:upass,pass2:upassr,firstname:ufname,lastname:ulname},form:id}));
        f.appendChild(snb);

        o.innerHTML='';
        o.appendChild(f);

    }
    function cineBoxFnProcessRegister(e,r,n,f){

        e.preventDefault();

        var cur;
        var dat={};
        for(var k in n){
            cur=document.getElementById(n[k]);
            dat[k]=cur.value;
        }

        var c=new cineBox();
        var t=cineBoxFnDomObject(e.target);

        var param = {
            reqType:'man',
            reqIdent:0,
            reqData:JSON.stringify(dat),
            reqAction:'userauth',
            reqQuery:'makeuserregister',
            reqEvent: cineBoxFnAfterLogin,
            reqEventData: true,
            reqWindow: false,
            reqTitle:'',
            reqAppTitle:"",
            reqBtnName:'',
            reqSpecial:f,
            reqExt:'api',
            reqRepeat:r
        };

        c.rexec.getManual(t,param);
    }
    function cineBoxFnRequireLogin(e,r){

        var uname='cine_fn_username';
        var upass='cine_fn_password';

        var f=document.createElement('form');
        f.className='cine_fn_form_default';
        var h='';

        h+='<label>Login or email</label>';
        h+='<input type="text" name="cine_fn_username" id="'+uname+'" value="">';
        h+='<label>Password</label>';
        h+='<input type="password" name="cine_fn_password" id="'+upass+'" value="">';

        var id= e.getAttribute('id');

        f.innerHTML=h;
        f.style.marginTop='40px';
        var snb=document.createElement('button');
        snb.className='cine_fn_color_btn';
        snb.style.width=200+'px';
        snb.setAttribute('type','button');
        snb.innerHTML='Login';
        snb.setAttribute('id','cine_login_button');
        snb.addEventListener('click',function(e){cineBoxFnProcessLogin(e,this.func,this.uname,this.upass,this.form)}.bind({func:r,uname:uname,upass:upass,form:id}));
        f.appendChild(snb);

        snb=document.createElement('h4');
        snb.innerHTML='If you have not registered yet you can sign up:';
        snb.style.width=200+'px';
        f.appendChild(snb);

        snb=document.createElement('button');
        snb.className='cine_fn_color_btn';
        snb.style.width=200+'px';
        snb.setAttribute('type','button');
        snb.innerHTML='Sign up';
        snb.setAttribute('id','cine_signup_button');
        snb.addEventListener('click',function(e){cineBoxFnRequireSignup(e,this.obj,this.ret,this.form)}.bind({obj:e,ret:r,form:id}));
        f.appendChild(snb);

        return f;
    }

    function cineBoxFnProcessLogin(e,r,n,p,f){

        e.preventDefault();

        var lg=document.getElementById(n);
        var pw=document.getElementById(p);

        XWF.connQueryObject = {query:"userauth",type:"makeuserlogin"};
        XWF.connQueryData = {ident:lg.value,ext:'api',data:pw.value,user:0};

        XWF.send({
            waitCallback:true,
            repeatFunction:r,
            onComplete:cineBoxFnAfterLogin
        });

    }

    function cineBoxFnAfterLogin(data){

        var d = data.data;

        if(d.success){

            var res= d.result;
            if(window.localStorage!=='undefined'){
                window.localStorage.setItem('user_id',res['user_id']);
                window.localStorage.setItem('user_key',res['user_key']);
            }

            if(XWF.privateViewCurrent!=null)
            {
                XWF.closewindow();
            }
            else
            {
                var obj=document.getElementsByClassName('cineRoot');
                if(obj.length>0){
                    cineFnClearDOM(obj[0]);
                }
            }

            data.repeat();

        } else {


        }

    }

    // COMPUTED STYLE TO INTEGER
    function cineBoxFnCoordPxRem(t){var r=/[(px+)(%+)]+/g;return parseInt(t.replace(r,''));}
    // OBJECT FAST INDEX SEARCH PROTOTYPES
    Array.prototype.indexElm = function(val){var l=this.length;for(var i=0;i<l;i++){if(this[i]===val)return {result:true,pos:i}}return false;};
    // Object.prototype.indexElm = function(val){for(var i in this){if(i===val)return true}return false;};
    // Rewrite object proto to function in case of jquery
    function cineObjectIndex(obj,val){for(var i in obj){if(i===val)return true}return false;};
    // OBJECT ARRAY CLEAN TO ONLY FILLED CELLS and RETURN ARRAY / LENGTH / STRING COMMA SEPARATED
    function cineBoxFnArrayFilled(a){var l=a.length; var e=l-1; var n=[]; var s=0; var v=''; console.log(a);
        for(var i=0;i<l;i++){
            if(a.hasOwnProperty(i)){
                if(a[i].length>0){
                    n[i]=a[i];
                    s++;
                    if(i<l-1)
                        v+=a[i]+',';
                    else
                        v+=a[i];
                }
            }
        }
        return {array:n,length:s,string:v};
    }
    // RETURN CLASS NAME OF FALSE
    function cineBoxFnReturnClass(s,c){
        var r=false;
        if(typeof(s)=='string'){s=s.split(' ');
            for(var i=0;i<s.length;i++){if(s[i]==c){r=s[i];}}
        }return r;}
    // DOM ELEMENTS (which can have value) TO JSON STRING
    function cineBoxFnEntityToJSON(obj){
        var i,l,z,r; r=false;
        if(Object.prototype.toString.call(obj) == '[object Array]'){l=obj.length;z=l-1;r='';
            for(i=0;i<l;i++){
                if(i!=z)
                    r+='"'+obj[i].id+'":"'+obj[i].value+'",';
                else
                    r+='"'+obj[i].id+'":"'+obj[i].value+'"';
            }
            r='{'+r+'}';
        }
        return r;
    }
    // FIND MODAL PARENT
    function cineBoxFnFindModal(obj){
        var tobj,cls='cinemodal';
        if(typeof obj === 'string'){
            obj = document.getElementById(obj);
        }
        if(typeof obj === 'object'){
            tobj=obj;
            do {
                if(cineBoxFnReturnClass(tobj.className,cls)){
                    return tobj;
                } else {
                    tobj=tobj.parentNode;
                }
            } while(tobj.tagName !== 'BODY');
        }
        return false;
    }

    // LOAD EXTERNAL LIBRARY
    function cineBoxFnloadScript(url,obj,callback,defer,deferfunc){
        var s = document.createElement("script");
        s.type = "text/javascript";
        if (s.readyState){
            s.onreadystatechange = function(){
                if (s.readyState == "loaded" || s.readyState == "complete"){
                    s.onreadystatechange = null;
                    callback(obj,defer,deferfunc);
                }
            };
        } else {
            s.onload = function(){callback(obj,defer,deferfunc);};
        }
        s.src = url;
        document.getElementsByTagName("head")[0].appendChild(s);
    }
    // DOM TREE TRAVERSE
    function cineBoxFnDomTraverse(obj,objclass){

        var cur,ti,curc,up,uppn;
        var t=['NONE','INPUT','SELECT','TEXTAREA'];
        var cl=false;

        if(objclass != null || typeof objclass !== 'undefined'){
            objclass = objclass.split(':');
            if(objclass[0]!=='*'){
                t=[objclass[0].toUpperCase()];}
            cl=objclass[1];
        }

        var d=[];

        if(obj!=null){

            var s=["none",obj.id];
            var c=obj.childNodes;
            var k=c.length;
            var z=0;

            for(var i=0;i<k;i++){
                cur = c[i];

                if(cur.nodeType != 3){

                    if(cur.id == null || cur.id == ''){
                        ti = new Date();
                        ti=ti.getHours()+''+ti.getMinutes()+''+ti.getSeconds()+''+ti.getMilliseconds()+''+Math.round((999)*Math.random());
                        cur.setAttribute('id','cine-fdt' + ti);
                    }

                    if(!s.indexElm(cur.id)){

                        if(!t.indexElm(cur.tagName)){

                            curc = cur.childNodes;
                            if(curc.length == 1 && curc[0].nodeType != 3){
                                c=curc; i=-1; k=c.length;
                            }
                            if(curc.length > 1){
                                c=curc; i=-1; k=c.length;
                            }
                        }
                        else {

                            if(cl){ // CLASS SPECIFIED
                                if(cineBoxFnReturnClass(cur.className,cl)){
                                    d.push(cur);}
                            }
                            else{ // NOT CLASS SPECIFIED
                                d.push(cur);}
                        }
                    }
                    s.push(cur.id);
                }
                if(i==(k-1)){
                    uppn=cur.parentNode;
                    if(uppn != obj){
                        up = uppn.parentNode;
                        c=up.childNodes;
                        i=-1;
                        k=c.length;
                    }
                }
                z++; if(z>1000) break;
            }

            return d;
        }
    }

    // Create list of ordered items with delete buttons
    function cineBoxFnRestoOrderItemList(r,id,key,fn,ds,ss){

        var i=0,cur,itm,elm,ec,et,eb,er,sd,sc;
        if(typeof fn==='undefined')fn=true;
        if(typeof ds==='undefined')ds=true;
        if(typeof ds==='undefined')ss=false;

        ec=document.createElement('table');
        ec.className='cine_fn_rol_i';
        ec.setAttribute('id','c_order_i_'+id);

        var p=0,p0=0,p1=0,p2=0;

        for(var k in r){
            if(r.hasOwnProperty(k)){

                cur=r[k];
                itm=Object.keys(cur);
                elm=cur[itm[0]];

                er=document.createElement('tr');
                er.className='cine_fn_rol_i_tr';
                er.setAttribute('id','cie_'+k);

                et=document.createElement('td');
                et.className='cine_fn_rol_i_td';
                et.setAttribute('valign','top');
                et.style.width='60%';

                bn = document.createElement('div');
                bn.className = 'cine_fn_iedit_cnt';
                if(fn){
                    bn.innerHTML = '<div class="cine_fn_iedit_btn"><i class="fa fa-cutlery"></i></div> &nbsp;' + elm.n + '';
                    bn.addEventListener('click', function (e) {
                        cineRestoOrderAddSide(e, this)
                    }.bind(k));
                }else
                    bn.innerHTML = elm.n;

                et.appendChild(bn);

                h='';

                var sext=false;
                if(ss){
                    sd=elm.s;
                    if(sd){

                        h+='<div>';
                        var skey=Object.keys(sd);
                        var lkey=skey[skey.length-1];

                        for(var s in sd){
                            sc=sd[s];
                            p0=0;
                            p1=Number(sc['p']);
                            p2=Number(sc['p2']);
                            if(s!=lkey)
                                cn="cine_fn_rol_i_tdsi";
                            else
                                cn="cine_fn_rol_i_tdsil";

                            if(p1>0)
                                p0+=p1;
                            if(p2>0)
                                p0+=p2;
                            if(p0>0)
                                n3='$ '+p0+'';
                            else
                                n3='';

                            n2=' &gt;'+sc['v']+'';
                            if(sc['p2']==null){
                                n2='';
                            }
                            h+='<div class="cine_fn_rol_i_tds"><div class="'+cn+'">'+sc['n']+' '+n2+'</div><div class="cine_fn_rol_i_tdsp">'+n3+'</div></div>';

                            sext=true;
                        }
                        h+='</div>';

                    }
                }

                et.insertAdjacentHTML('beforeend',h);
                er.appendChild(et);
                if(!sext){er.className='cine_fn_rol_i_trl'}

                et=document.createElement('td');
                et.className='cine_fn_rol_i_tq';
                et.setAttribute('id','ciq_'+k);
                et.setAttribute('valign','top');
                et.innerHTML='<h4> x&nbsp;'+elm.q+'</h4>';
                er.appendChild(et);

                et=document.createElement('td');
                et.className='cine_fn_rol_i_tp';
                et.setAttribute('valign','top');
                et.innerHTML='<h4>$ '+elm.p+'</h4>';
                er.appendChild(et);

                if(fn){
                    et=document.createElement('td');
                    et.className='cine_fn_rol_i_tb';
                    et.setAttribute('valign','top');
                    eb=document.createElement('button');
                    eb.className='cine_fn_color_btn cine_common_btn';
                    eb.innerHTML='<div class="c-btn-delete"></div>';
                    eb.addEventListener('click',function(e){cineBoxFnRestoOrderIdelete(e,this.id,this.nid,this.oid,this.key)}.bind({id:k,nid:itm[0],oid:id,key:key}));
                    et.appendChild(eb);
                    er.appendChild(et);
                }

                ec.appendChild(er);

                i++;
            }
        }
        if(i>0) return ec; return false;
    }

    // Create a summary for item checkout
    function cineBoxFnRestoOrderItemSumm(p,t,u){

        $cine().order.countPrices(p,t,u);

        var i=0;

        var ec=document.createElement('table');
        ec.className='cine_fn_rol_i_total font16 full-width';
        ec.setAttribute('id','cine_fn_order_total');

        var er=ec.insertRow(i);
        er.className='cine_fn_rol_i_total_s';
        var et=er.insertCell(0);
        et.textContent='Subtotal';
        et=er.insertCell(1);
        et.setAttribute('id','cine_fn_rol_itotal');
        et.className='cine_fn_rol_i_price';
        et.textContent='$ '+ $cine().order.price(astype.string);
        i++;

        //var pt=((p/100)*(t));

        er=ec.insertRow(i);
        er.className='cine_fn_rol_i_total_s';
        et=er.insertCell(0);
        et.textContent='Tax';

        et=er.insertCell(1);
        et.setAttribute('id','cine_fn_rol_ttotal');
        et.className='cine_fn_rol_i_price';
        et.textContent='$ ' + $cine().order.tax(astype.string);
        i++;

        //var pf=Number(pt)+Number(p);
        if(typeof u !=='undefined'){

            er=ec.insertRow(i);
            er.className='cine_fn_rol_i_total_s';
            et=er.insertCell(0);
            et.textContent='Customer tip';
            et=er.insertCell(1);
            et.className='cine_fn_rol_i_price';
            et.setAttribute('id','cine_fn_rol_utotal');

            et.textContent='$'+ $cine().order.tip(astype.string);
            i++;
        }

        er=ec.insertRow(i);
        er.style.cssText='border-top:1px solid #cccccc; padding-top:4px;';
        er.className='cine_fn_rol_i_total_s';

        et=er.insertCell(0);
        et.innerHTML='<h4>Total</h4>';
        et=er.insertCell(1);
        et.setAttribute('id','cine_fn_rol_ototal');
        et.className='cine_fn_rol_i_price';
        et.innerHTML='<h4>$ '+$cine().order.total(astype.string)+'</h4>';

        return ec;
    }

    function cineBoxFnRestoOrderIdelete(e,id,nid,oid,key){

        var c=new cineBox();
        var t=cineBoxFnDomObject(e.target);

        var data=JSON.stringify({order:oid,item:id,subitem:nid});

        var param = {
            reqType:'man',
            reqIdent:oid,
            reqAction:'restoorder',
            reqQuery:'delitemfromorder',
            reqEvent: cineBoxFnRestoOrderIdeleteProc,
            reqEventData: true,
            reqWindow: false,
            reqTitle:'',
            reqBtnName:false,
            reqSpecial:'',
            reqData: data,
            reqExt:key
        };
        c.rexec.getManual(t,param);

    }

    function cineBoxFnRestoOrderIdeleteProc(e,r){

        var data= r.result;

        var elem= document.getElementById('cie_'+data.item);
        var elemq= document.getElementById('ciq_'+data.item);

        var ttlp= document.getElementById('cine_fn_rol_itotal');
        var ttlt= document.getElementById('cine_fn_rol_ttotal');
        var ttlo= document.getElementById('cine_fn_rol_ototal');

        if(data.q>0){
            elemq.innerHTML='x&nbsp;'+data.q;
        } else {
            elem.parentNode.removeChild(elem);
        }

        var pt=((data.p/100)*(8.75)).toFixed(2);
        var pto=Number(pt)+Number(data.p);

        ttlp.innerHTML='$ '+data.p.toFixed(2);
        ttlt.innerHTML='$ '+pt;
        ttlo.innerHTML='$ '+pto.toFixed(2);

    }


    /* -----------------------------------------------
     * * * * * * * * * * * * * * * * * * * * * * * * *
     ADDITIONAL FUNCTIONAL FOR ORDERS
     type = functions
     * * * * * * * * * * * * * * * * * * * * * * * *
     ------------------------------------------------ */
    function cOrderPriceCount(p,t,u){
        p=Number(p);
        t=(t==0 || typeof t==='undefined')? 8.875 : Number(t);
        u=(typeof u==='undefined')?0:Number(u);
        tax=Number(((p/100)*t).toFixed(2));
        tip=(p/100)*u;
        total=p+tax+tip;
        $cine().order.setNumbers(p,tax,tip,total,t,u);
    }

    // ----------------------------------------------------------------------------------------------------------------
    // SUPPORT FUNCTIONS FOR RESTAURANTS AND RESTAURANTS MENU
    // ----------------------------------------------------------------------------------------------------------------

    function cineBoxFnRestoIPriceCnt(d){
        var p=0,v,vc,vp=0,svp=0;
        p=Number(d.p);
        v=d.s;

        if(v && typeof v==='object'){
            for(var i in v){
                if(v.hasOwnProperty(i)){
                    vc=v[i];
                    if(vc.p>0)
                        vp+=Number(vc.p);
                    if(vc.p2>0)
                        svp+=Number(vc.p2);
                }
            }
        }

        return p+vp+svp;
    }
    // ----------------------------------------
    // ---- Restaurant menu back function -----
    // ----------------------------------------
    Array.prototype.remove = function(from, to) {
        var rest = this.slice((to || from) + 1 || this.length);
        this.length = from < 0 ? this.length + from : from;
        return this.push.apply(this,rest);
    };

    function cineBlockButton(e){
        var c;
        if(e!=null){
            c=e.className
            e.className += ' disabled';
            setTimeout(function () {
                var e = this.o;
                e.className = this.c;
            }.bind({o: e, c: c}), 1000);
        }
    }

    function cineBoxLabelFrame(o){var e,i,l,k,d;
        e = document.createElement('div'); e.className='cine_fn_domframe';
        i = document.createElement('div'); i.className='cine_fn_domframe_l';
        i.innerHTML=o.title;
        e.appendChild(i);
        i = document.createElement('div'); i.className='cine_fn_domframe_c';
        if(o.hasOwnProperty('elems')){
            d=o.elems;

            if(d.nodeType===1){
                i.appendChild(d);
            }else{
                l=d.length;
                if(l>0){
                    for(k=0;k<l;k++){
                        if(typeof d[k]==='object')
                            i.appendChild(d[k]);
                        else
                            i.insertAdjacentHTML('beforeend',d[k]);
                    }
                }
            }
        }else{
            i.appendChild(o);
        }
        e.appendChild(i);
        return e;
    }
    function cineBoxFnDomObject(e,type){
        var obj;
        var ev=false;
        if(e.target){
            obj=e.target;
            if(e.preventDefault)
                ev=e;
        }
        else if (typeof e==='string'){
            obj=document.getElementById(e);
        } else
            obj=e;

        if(typeof type !== 'undefined'){
            if(obj.tagName == type)
                return obj;
            else
                return obj.parentNode;
        }
        else
            return {object:obj,event:ev};
    }
    function cineCreateDomEl(t,i,c,h,p){

        var e=document.createElement(t);
        e.setAttribute('id',i);
        e.className=c;
        if(h.length>0 && e.nodeName=='INPUT'){
            e.setAttribute('value',h);
        } else {
            e.innerHTML = h;
        }
        if(typeof p==='object'){
            p.appendChild(e);
        }
        return e;

    }
    function cineMakeDays(d){

        var dw=['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
        var dp=[31,28,31,30,31,30,31,31,30,31,30,31];
        var mt=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        var od=['Today','Tomorrow','','','','',''];

        var cd=d.getUTCDay();
        var md= d.getUTCDate();
        var ms= d.getUTCMonth();
        var ys= d.getUTCFullYear();

        var i, k, c, m, y, h='', rd,rm;
        k=cd;
        c=md;
        m=ms;
        y=ys;
        for(i=0;i<7;i++){

            if(k>6)k=0;

            if(c>dp[m]){
                c=1;
                m++;
            }

            if(c<10)
                rd='0'+c;
            else
                rd=c;

            if(m>12){
                y++;
            }

            rm=m+1;
            if(rm<10)
                rm='0'+rm;

            if(i>1){
                h+='<option value="'+y+'-'+rm+'-'+rd+'">'+od[i]+' '+dw[k]+' - '+mt[m]+' '+c+' </option>';
            } else {
                h+='<option value="'+y+'-'+rm+'-'+rd+'">'+od[i]+' - '+mt[m]+' '+c+' </option>';
            }

            c++;
            k++;

        }

        ms++;
        if(ms<10)
            ms='0'+ms;
        if(md<10)
            ms='0'+md;
        return {data:h,year:ys,month:ms,day:md};
    }

    // JSON TEXT CHECK FOR QOUTES
    function cineJsonTextCheck(t){
        var s = {'&':'&amp;','<':' &lt;','>':'&gt; ','\'':'\x27','"':'\\\x22','\\':' '};
        t = t.replace(/[\\\"'&]|(<h3>)|(<\/h3>)|(<)|(>)/ig,function(e){return s[e] || e;});
        t = t.replace(/\r?\n/g,'<br />');
        return t;
    }

    if(!window.$cine){window.$cine = $cine;}
})();

function cOpenTinyModal(s){

    var o=document.createElement('div');
    o.className='fadeInDown';
    var u=document.createElement('div');
    var title=false,ptitle='';

    var h=0;
    if(XWF.windowHeight<=560){
        h=560/100*83;
        sh='position:relative; width:100%; height:'+h+'px; text-align:center;';
        h=XWF.windowHeight-90;
    }
    else{
        h=Math.floor(XWF.windowHeight/100*83);
        sh='position:relative; width:100%; height:100%; text-align:center;';
    }

    o.style.cssText='position:absolute; width:90%; height:'+h+'px; padding:10px; margin:90px 5% 0 5%; top:0; left:0; background:#ffffff; border-radius:8px; overflow-y:scroll';
    u.style.cssText='position:absolute; width:100%; height:'+XWF.windowHeight+'px; top:0; left:0;';
    u.className='c_modal_fancy_bg';

    var f=document.createElement('div');
    f.style.cssText=sh;
    o.appendChild(f);

    if(s.appTitle)
    {
        title=true;
        var titleLayer=document.createElement('div');
        titleLayer.style.cssText = 'position: absolute; top:34px; left:0;';
        titleLayer.innerHTML='<span id="cine_fn_ctitle" class="navbar-brand" style="max-width:220px; position: relative; overflow: hidden; line-height:32px; margin-top: -6px;"> '+s.appTitle+' </span>';
        u.appendChild(titleLayer);
    }

    var inititator=null;

    if(s.initiator){
        inititator= s.initiator;
    }

    var r={modal:o,under:u,content:f,title:ptitle,titleChange:title,initiator:inititator};

    if(s.buttonShow)
    {
        c = document.createElement('div');
        c.style.cssText = 'width:100%; height:50px; text-align:center; position:absolute; bottom:8px;';

        if(s.hasOwnProperty('buttonName')) {

            closeWindow=true;
            if(s.hasOwnProperty('buttonCloseWindow')) {
                closeWindow= s.buttonCloseWindow;
            }

            b = document.createElement('button');
            b.className = "cine_fn_color_btn cine_short_btn";
            b.style.cssText = 'width:90% !important;';
            b.innerHTML = s.buttonName;
            b.addEventListener('click', function (e) {

                if (typeof s.buttonEvent === 'function') {
                    s.buttonEvent(e);
                }

                if(closeWindow)
                {
                    this.under.style.display = 'none';
                    this.under.parentNode.removeChild(this.under);
                    this.modal.parentNode.removeChild(this.modal);
                    cineFnCurrentModal = null;
                }

            }.bind(r));
            c.appendChild(b);
            r['buttonNormal'] = b;

        }

        if(s.hasOwnProperty('buttonCancel')){
            c.style.height="96px";
            b=document.createElement('button');
            b.className="cine_fn_color_btn cine_short_btn";
            b.style.cssText='width:90% !important; margin-top:0px !important;';
            b.innerHTML= 'Cancel';
            b.addEventListener('click',function(e){

                if(title){
                    atl=document.getElementById('cine_fn_ctitle');
                    atl.innerHTML=this.title;
                }
                this.under.style.display='none';
                this.under.parentNode.removeChild(this.under);
                this.modal.parentNode.removeChild(this.modal);
                cineFnCurrentModal=null;

            }.bind(r));
            c.appendChild(b);
            r['buttonCancel']=b;
        }

        f.appendChild(c);
        r['button']=c;
    }

    document.body.appendChild(u);
    document.body.appendChild(o);
    cineFnCurrentModal=r;
    return r;
}

function cRemoveTinyModal()
{
    if(cineFnCurrentModal!=null && document.contains(cineFnCurrentModal.modal)){

        if(cineFnCurrentModal.titleChange){
            atl=document.getElementById('cine_fn_ctitle');
            atl.innerHTML=cineFnCurrentModal.title;
        }
        cineFnCurrentModal.modal.parentNode.removeChild(cineFnCurrentModal.modal);
        cineFnCurrentModal.under.parentNode.removeChild(cineFnCurrentModal.under);

        cineFnCurrentModal=null;
    }
}

function cineFnClearDOM(node){
    if(node!=null){
        while (node.firstChild){
            node.removeChild(node.firstChild);
        }
    }
}

function cTimeObject(s){
    if(typeof s!=='undefined'){
        var e=s.split(' ');
        var d=e[0];
        var t=e[1];
        if(typeof d!=='undefined')
            d=d.split('-');
        else
            d=['0000','00','00'];
        if(typeof t!=='undefined') {
            t = t.split(':');
            t[2]=(typeof t[2]==='undefined')?'00':t[2];
        }
        else
            t=['00','00','00'];
        tMin=Number(t[1]);
        tHor=Number(t[0]);
        tSec=Number(t[2]);
        dDay=Number(d[2]);
        dMon=Number(d[1]);
        return {year:d[0],month:(dMon<10)?'0'+dMon:dMon,day:(dDay<10)?'0'+dDay:dDay,hour:(tHor<10)?'0'+tHor:tHor,minute:(tMin<10)?'0'+tMin:tMin,second:(tSec<10)?'0'+tSec:tSec,
            y:Number(d[0]),m:dMon,d:dDay,h:tHor,i:tMin,s:tSec};
    }
}
function cineTimeNormalize(h,m){
    var nh=h%12;
    if(nh==0)nh=12;
    if(typeof m==='string')
        return nh+":"+m+(h<12?' am':' pm');
    else
        return nh+":"+(m<10?"0"+m:m)+(h<12?' am':' pm');
}

function clone(obj) {
    var target = {};
    for (var i in obj) {
        if (obj.hasOwnProperty(i)) {
            target[i] = obj[i];
        }
    }
    return target;
}

XWF.connection({
    server:"https://api.myvoila.me/srv",
    //server:'http://eatnow1212/srv',
    serverMethod:"url",
    eventOnError:$cine().error,
    queryObject:{
        query:"", type:""
    },
    queryData:{
        user:0, userkey:"", ident:0
    }
});

var clearServer=XWF.connServer.replace('srv','');
var appImagePath=clearServer+'img/';
